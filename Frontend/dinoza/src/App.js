import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import Navbar from '../src/components/Navbar.js';
import Footer from '../src/components/Footer.js';
import Header from '../src/components/Header.js';
import AboutUs from '../src/components/AboutUs.js';
import Gallery from '../src/components/Gallery.js';
import Contact from '../src/components/Contact.js';
import Login from '../src/components/Login.js';
import Register from '../src/components/Register.js';
import Home from '../src/components/Home.js';
import MissingPage from '../src/components/MissingPage.js';
import UserView from '../src/components/User/ViewUsers.js'
import AddClient from '../src/components/Client/AddClient.js';
import ViewClients from '../src/components/Client/ViewClients.js';
import AddSupplier from '../src/components/Supplier/AddSupplier.js';
import ViewSuppliers from '../src/components/Supplier/ViewSupplier.js';
import AddInvoice from '../src/components/Invoice/AddInvoice';
import ViewInvoice from '../src/components/Invoice/ViewInvoices.js';
import InvoiceDetails from '../src/components/Invoice/InvoiceDetails.js';
import AddOrder from '../src/components/Order/AddOrder.js';
import ViewOrder from '../src/components/Order/ViewOrders.js';
import OrderDetails from '../src/components/Order/OrderDetails.js';
import AddProduction from '../src/components/Production/AddProduction.js';
import ViewProduction from '../src/components/Production/ViewProductions.js';
import ProductionDetails from '../src/components/Production/ProductionDetails.js';
import ViewNotifications from '../src/components/Notification/NotificationsMain';
function App() {
  return (
    <Router>
      <div>
        <Navbar />
        <Routes>
          <Route path="*" element={<MissingPage />} />
          <Route path="/" element={<Landing />} />
          <Route path="login" element={<Login />} />
          <Route path ="register" element={<Register />} />
          <Route path ="home" element={<Home/>}/>
          <Route path = "/user/view" element = {<UserView />} />
          <Route path = "/client/add" element = {<AddClient />} />
          <Route path = "/client/view" element = {<ViewClients />} />
          <Route path = "/supplier/add" element = {<AddSupplier />} />
          <Route path = "/supplier/view" element = {<ViewSuppliers />} />
          <Route path = "/invoice/add" element = {<AddInvoice />} />
          <Route path = "/invoice/view" element = {<ViewInvoice />} />
          <Route path = "/invoice/view/:id" element = {<InvoiceDetails />} />
          <Route path = "/order/add" element = {<AddOrder/>} />
          <Route path = "/order/view" element = {<ViewOrder/>} />
          <Route path = "/order/view/:id" element={<OrderDetails />} />
          <Route path = "/production/add" element = {<AddProduction />} />
          <Route path = "/production/view" element = {<ViewProduction />} />
          <Route path = "/production/view/:id" element = {<ProductionDetails />} />
          <Route path = "/notifications" element = {<ViewNotifications />} />
        </Routes>
      </div>
    </Router>
  );
}

function Landing() {
  return (
    <>
      <Header />
      <AboutUs />
      <Gallery />
      <Contact />
      <Footer />
    </>
  );
}

export default App;
