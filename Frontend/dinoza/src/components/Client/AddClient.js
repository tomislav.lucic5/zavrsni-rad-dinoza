import React, { useState } from 'react';
import axios from 'axios';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import '../../css/AddDataForm.css'; // Add your custom CSS for styling
import Sidebar from '../Sidebar.js';

const AddDataForm = () => {
  const [formData, setFormData] = useState({
    name: '',
    paymentType: '',
    street: '',
    city: '',
    zip: '',
    country: '',
    oib: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Make Axios POST request to your API
    axios.post('https://localhost:44368/api/Client', formData)
      .then((response) => {
        console.log('Data added successfully', response.data);
        // Reset the form data
        setFormData({
          name: '',
          paymentType: '',
          street: '',
          city: '',
          zip: '',
          country: '',
          oib: '',
        });
      })
      .catch((error) => {
        console.error('Error adding data', error);
      });
  };

  return (
    
    <div className="add-data-form">
        <Sidebar />
      <h2>ADD CLIENT</h2>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Name"
          name="name"
          value={formData.name}
          onChange={handleChange}
          required
        />
        <TextField
          label="Payment Type"
          name="paymentType"
          value={formData.paymentType}
          onChange={handleChange}
          required
        />
        <TextField
          label="Street"
          name="street"
          value={formData.street}
          onChange={handleChange}
          required
        />
        <TextField
          label="City"
          name="city"
          value={formData.city}
          onChange={handleChange}
          required
        />
        <TextField
          label="Zip"
          name="zip"
          value={formData.zip}
          onChange={handleChange}
          required
        />
        <TextField
          label="Country"
          name="country"
          value={formData.country}
          onChange={handleChange}
          required
        />
        <TextField
          label="OIB"
          name="oib"
          value={formData.oib}
          onChange={handleChange}
          required
        />
        <Button variant="contained" color="primary" type="submit">
          submit
        </Button>
      </form>
    </div>
  );
};

export default AddDataForm;
