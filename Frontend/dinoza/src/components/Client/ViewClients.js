import React from 'react';
import Sidebar from '../Sidebar';
import ClientTable from './ClientTable'; 
import '../../css/UserTable.css';

const ViewClients = () => {
    const role = localStorage.getItem('role');
    const loggedInUser = role === 'User' ? 'Client' : 'Admin';
  
  return (
    <div className="view-user">
      <Sidebar />
      <div className="main-content" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
        <h2 className="endpoint-heading">client/view</h2>
        <hr></hr>
        <ClientTable /> 
      </div>
    </div>
  );
};

export default ViewClients;
