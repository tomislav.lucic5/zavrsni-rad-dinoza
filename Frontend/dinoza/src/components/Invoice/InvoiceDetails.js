import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import '../../css/OrderDetails.css'; // Import your custom CSS file
import Sidebar from '../Sidebar.js';

const InvoiceDetails = () => {
  const { id } = useParams();
  const [invoiceDetails, setInvoiceDetails] = useState(null);
  const [editableStatus, setEditableStatus] = useState(false);
  const [newStatus, setNewStatus] = useState('');
  const [statuses, setStatuses] = useState([]);

  useEffect(() => {
    // Fetch statuses and warehouses from API
    axios.get('https://localhost:44368/api/Status')
      .then(response => {
        setStatuses(response.data);
      })
      .catch(error => {
        console.error('Error fetching statuses:', error);
      });

    // Fetch order details
    axios.get(`https://localhost:44368/api/Invoice/${id}`)
      .then(response => {
        setInvoiceDetails(response.data[0]);
        setNewStatus(response.data[0].statusName);
      })
      .catch(error => {
        console.error('Error fetching order details:', error);
      });
  }, [id]);

  const handleStatusChange = () => {
    const selectedStatus = statuses.find(status => status.name === newStatus);

    if (!selectedStatus) {
      console.error('Selected status not found');
      return;
    }

   
    axios.put(`https://localhost:44368/api/Invoice/${id}`, {
      statusId: selectedStatus.id,
    })
      .then(response => {
        setInvoiceDetails({
          ...invoiceDetails,
          statusName: newStatus
        });
        setEditableStatus(false);
      })
      .catch(error => {
        console.error('Error updating status:', error);
      });
  };

  if (!invoiceDetails) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <div className="order-card">
        <Sidebar />
        <h2>#{invoiceDetails.invoiceNumber}</h2>
        <hr />
        <div className="order-info">
          <div className="info-row">
            <p className="info-label">Client Name:</p>
            <p className="info-value">{invoiceDetails.clientName}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Production Date:</p>
            <p className="info-value">{new Date(invoiceDetails.dateCreated).toLocaleDateString()}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Price:</p>
            <p className="info-value">{invoiceDetails.price}€</p>
          </div>
          <div className="info-row">
            <p className="info-label">Production Status:</p>
            <p className="info-value">
              {editableStatus ? (
                <>
                  <select
                    value={newStatus}
                    onChange={(e) => setNewStatus(e.target.value)}
                  >
                    {statuses.map(status => (
                      <option key={status.id} value={status.name}>
                        {status.name}
                      </option>
                    ))}
                  </select>
                  <button onClick={handleStatusChange}>Save</button>
                </>
              ) : (
                <>
                  {invoiceDetails.statusName}
                  <button onClick={() => setEditableStatus(true)}>Edit</button>
                </>
              )}
            </p>
          </div>
          <hr />
          <div className="info-row">
            <p className="info-label">Manufacturer:</p>
            <p className="info-value">{invoiceDetails.manufacturerName}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Invoice Creator Name:</p>
            <p className="info-value">{`${invoiceDetails.invoiceCreatorName} ${invoiceDetails.invoiceCreatorLastName}`}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Invoice Status:</p>
            <p className="info-value">{invoiceDetails.statusName}</p>
          </div>
        </div>
      </div>
    </div>
  );
  
};

export default InvoiceDetails;
