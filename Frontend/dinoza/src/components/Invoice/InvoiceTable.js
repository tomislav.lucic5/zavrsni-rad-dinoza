import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../../css/UserTable.css';

const TableComponent = () => {
     const [searchQuery, setSearchQuery] = useState('');
     const [data, setData] = useState([]);

  useEffect(() => {

    axios.get('https://localhost:44368/api/Invoice')
      .then(response => {
        setData(response.data);
        console.log(response.data);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);
  const filteredData = data.filter(invoice =>
    invoice.invoiceNumber.toString().includes(searchQuery)
  );
  
  return (
    <div className="table-container">
        <div className="search-container">
        <input
            className="search-input"
            type="text"
            placeholder="Search by Invoice Number"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
        />
        </div>
  <table className="table">
    <thead>
      <tr>
        <th>Invoice Number</th>
        <th>Client Name</th>
        <th>Manufacturer Name</th>
        <th>Resource price</th>
        <th>Invoice date</th>
        <th>Invoice creator</th>
        <th>Invoice status</th>
      </tr>
    </thead>
    <tbody>
      {filteredData.map(invoice => (
        <tr key={invoice.id}>
          <td>
                <Link to={`/invoice/view/${invoice.id}`} className="no-decoration-link">
                  #{invoice.invoiceNumber}
                </Link>
              </td>
          <td>{invoice.clientName}</td>
          <td>{invoice.manufacturerName}</td>
          <td>{invoice.price}€</td>
          <td>{new Date(invoice.dateCreated).toLocaleDateString()}</td>
          <td>{invoice.invoiceCreatorName} {invoice.invoiceCreatorLastName}</td>
          <td>{invoice.statusName}</td>
        </tr>
      ))}
    </tbody>
  </table>
  <p className="total-users">Total invoices: {data.length}</p>
</div>

  );
};

export default TableComponent;
