import React from 'react';
import Sidebar from '../Sidebar';
import InvoiceTable from './InvoiceTable'; 
import '../../css/UserTable.css';

const ViewInvoices = () => {
    const role = localStorage.getItem('role');
    const loggedInUser = role === 'User' ? 'Client' : 'Admin';
  
  return (
    <div className="view-user">
      <Sidebar />
      <div className="main-content" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
        <h2 className="endpoint-heading">invoice/view</h2>
        <hr></hr>
        <InvoiceTable /> 
      </div>
    </div>
  );
};

export default ViewInvoices;
