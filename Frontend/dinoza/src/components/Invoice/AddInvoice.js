import React, { useState, useEffect } from 'react';
import axios from 'axios';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import '../../css/AddDataForm.css'; // Add your custom CSS for styling
import Sidebar from '../Sidebar.js';

const AddDataForm = () => {
  const [formData, setFormData] = useState({
    clientId: '',
    invoiceNumber: '',
    manufacturerId: 'fba571ce-3753-482f-bf85-6550b38f7b98',
    materialProductionId: '',
    invoiceCreatorName: '',
    invoiceCreatorLastName: '',
    statusId: '',
  });

  const [clients, setClients] = useState([]);
  const [statuses, setStatuses] = useState([]);
  const [materialProductions, setMaterialProductions] = useState([]);

  useEffect(() => {
    axios.get('https://localhost:44368/api/Client')
      .then(response => {
        setClients(response.data);
      })
      .catch(error => {
        console.error('Error fetching clients:', error);
      });

      axios.get('https://localhost:44368/api/Status')
      .then(response => {
        setStatuses(response.data);
      })
      .catch(error => {
        console.error('Error fetching statuses:', error);
      });

      axios.get('https://localhost:44368/api/MaterialProduction')
      .then(response => {
        setMaterialProductions(response.data);
      })
      .catch(error => {
        console.error('Error fetching statuses:', error);
      });

  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    axios.post('https://localhost:44368/api/Invoice', formData)
      .then((response) => {
        console.log('Data added successfully', response.data);
        setFormData({
          clientId: '',
          invoiceNumber: '',
          manufacturerId: 'fba571ce-3753-482f-bf85-6550b38f7b98',
          materialProductionId: '',
          invoiceCreatorName: '',
          invoiceCreatorLastName: '',
          statusId: '',
        });
      })
      .catch((error) => {
        console.error('Error adding data', error);
      });
  };

  return (
    <div className="add-data-form">
      <Sidebar />
      <h2>ADD INVOICE</h2>
      <form onSubmit={handleSubmit}>
        <FormControl>
          <InputLabel>Client</InputLabel>
          <Select
            label="Client"
            name="clientId"
            value={formData.clientId}
            onChange={handleChange}
            required
          >
            {clients.map(client => (
              <MenuItem key={client.id} value={client.id}>
                {client.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <TextField
          label="Invoice Number"
          name="invoiceNumber"
          value={formData.invoiceNumber}
          onChange={handleChange}
          required
        />
        <FormControl className="select-container select-margin">
        <InputLabel>Manufacturer</InputLabel>
        <Select
            label="Manufacturer"
            disabled
            value="Dinoza"
        >
            <MenuItem value="Dinoza">Dinoza</MenuItem>
        </Select>
        </FormControl>
        <TextField
          label="Invoice Creator Name"
          name="invoiceCreatorName"
          value={formData.invoiceCreatorName}
          onChange={handleChange}
          required
        />
        <FormControl>
          <InputLabel>Production Reference</InputLabel>
          <Select
            label="MaterialProduction"
            name="materialProductionId"
            value={formData.materialProductionId}
            onChange={handleChange}
            required
          >
            {materialProductions.map(materialProduction => (
              <MenuItem key={materialProduction.id} value={materialProduction.id}>
                #{materialProduction.productionNumber}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <TextField
          label="Invoice Creator Last Name"
          name="invoiceCreatorLastName"
          value={formData.invoiceCreatorLastName}
          onChange={handleChange}
          required
        />
        <FormControl>
          <InputLabel>Status</InputLabel>
          <Select
            label="Status"
            name="statusId"
            value={formData.statusId}
            onChange={handleChange}
            required
          >
            {statuses.map(status => (
              <MenuItem key={status.id} value={status.id}>
                {status.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button variant="contained" color="primary" type="submit">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default AddDataForm;
