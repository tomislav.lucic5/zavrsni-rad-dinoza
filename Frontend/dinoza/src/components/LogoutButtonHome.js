import React from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import '../css/Navbar.css';

function LogoutForm() {
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      localStorage.removeItem('token');
      localStorage.removeItem('role');
      navigate('/');
    } catch (error) {
  
      console.error('Logout error:', error);
    }
  };

  return (
    <form onSubmit={handleLogout}>
      <button className="login-button" type="submit">Logout</button>
    </form>
  );
}

export default LogoutForm;
