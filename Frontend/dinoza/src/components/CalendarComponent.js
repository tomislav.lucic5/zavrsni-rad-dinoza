import React, { useState, useEffect } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import '../css/CalendarComponent.css';

const CalendarComponent = () => {
  const [date, setDate] = useState(new Date());
  const [selectedNote, setSelectedNote] = useState('');
  const [notes, setNotes] = useState({});

  useEffect(() => {
    // Retrieve notes from local storage on component mount
    const storedNotes = JSON.parse(localStorage.getItem('userNotes')) || {};
    setNotes(storedNotes);
  }, []);

  const handleDateChange = newDate => {
    setDate(newDate);
  };

  const handleNoteChange = event => {
    setSelectedNote(event.target.value);
  };

  const addNote = () => {
    if (selectedNote.trim() !== '') {
      const updatedNotes = { ...notes, [date.toISOString()]: selectedNote };
      setNotes(updatedNotes);
      setSelectedNote('');

      // Store notes in local storage
      localStorage.setItem('userNotes', JSON.stringify(updatedNotes));
    }
  };

  return (
    <div className="calendar-container">
      <div className="calendar">
        <Calendar onChange={handleDateChange} value={date} />
      </div>
      <div className="notes">
        <textarea
          placeholder="Add a note..."
          value={selectedNote}
          onChange={handleNoteChange}
        />
        <button onClick={addNote}>Add Note</button>
      </div>
      <div className="selected-notes">
        {notes[date.toISOString()] && (
          <div className="selected-note">
            <h3>Selected Date: {date.toDateString()}</h3>
            <p>{notes[date.toISOString()]}</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default CalendarComponent;
