import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../../css/UserTable.css';

const TableComponent = () => {
  const [data, setData] = useState([]);
  const [unreadNotifications, setUnreadNotifications] = useState(0);
  const [clickedRow, setClickedRow] = useState(null);

  useEffect(() => {
    // Fetch data using Axios
    axios.get('https://localhost:44368/api/Notification')
      .then(response => {
        setData(response.data);
        const unreadNotifications = response.data.filter(notification => !notification.isRead);
        setUnreadNotifications(unreadNotifications.length);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, [clickedRow]);

  const handleRowClick = async (notification) => {
    if (!notification.isRead) {
      try {
        await axios.put(`https://localhost:44368/api/Notification/${notification.id}`, { isRead: true });
        setClickedRow(notification.id);
      } catch (error) {
        console.error('Error updating notification:', error);
      }
    }
  };
  
  return (
    <div className="table-container">
      <p className="total-users">Unread notifications: {unreadNotifications}</p>
      <table className="table">
        <thead>
          <tr>
            <th>Title</th>
            <th>Body</th>
            <th>Date Created</th>
            <th>Importance</th>
          </tr>
        </thead>
        <tbody>
          {data.map(notification => (
            <tr
              key={notification.id}
              className={!notification.isRead && clickedRow !== notification.id ? 'unread-row' : ''}
              onClick={() => handleRowClick(notification)}
            >
              <td>{notification.title}</td>
              <td>{notification.body}</td>
              <td>{new Date(notification.dateCreated).toLocaleDateString()}</td>
              <td className={notification.importance === 'Urgent' ? 'red-text' : 'green-text'}>
                {notification.importance}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <p className="total-users">Total notifications: {data.length}</p>
    </div>
  );
};

export default TableComponent;
