import React from 'react';
import Sidebar from '../Sidebar';
import ViewNotifications from './ViewNotifications.js';
import '../../css/UserTable.css';

const NotificationsMain = () => {
    const role = localStorage.getItem('role');
    const loggedInUser = role === 'User' ? 'Client' : 'Admin';
  
  return (
    <div className="view-user">
      <Sidebar />
      <div className="main-content" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
        <h2 className="endpoint-heading">notifications</h2>
        <hr></hr>
        <ViewNotifications /> 
      </div>
    </div>
  );
};

export default NotificationsMain;
