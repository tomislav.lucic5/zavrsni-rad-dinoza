import React from 'react';
import Sidebar from '../Sidebar';
import NotificationTable from './NotificationTable'; 
import '../../css/UserTable.css';

const ViewNotifications = () => {
  
  return (
    <div className="view-user">
      <Sidebar />
      <div className="main-content" >
        <NotificationTable /> 
      </div>
    </div>
  );
};

export default ViewNotifications;
