import React from 'react';
import { Link, useLocation } from 'react-router-dom';

import '../css/Navbar.css'; // Import your CSS file for styling
import LogoutButtonHome from './LogoutButtonHome.js';

function Navbar() {
  const token = localStorage.getItem("token"); // Get the token from local storage
  const role = localStorage.getItem("role");

  const location = useLocation();
  return (
    <nav className="navbar">
      <div className="navbar-container">
        {((location.pathname === '/' || location.pathname === '/home' && role !== "Admin") || !token)  &&  (
          <Link to="/">
            <img src="/img/logo.png" alt="My Image" className="logo" />
          </Link>
        ) }
        <ul className="nav-menu">
          {!token && (
            <li className="nav-item">
              <Link to="/login">
                <button className="login-button">Log In</button>
              </Link>
            </li>
          )}
          {!token && (
            <li className="nav-item">
              <Link to="/register">
                <button className="signup-button">Sign Up</button>
              </Link>
            </li>
          )}
  
          {token && (
            <>
              {(location.pathname === '/' || location.pathname === '/home' && role !== "Admin") && (
                <li className="nav-item">
                  <LogoutButtonHome/>
                </li>
              )}
              {location.pathname === '/' && (
                <li className="nav-item">
                  <Link to="/home">
                    <button className="login-button">Dashboard</button>
                  </Link>
                </li>
              )}
            </>
          )}
        </ul>
      </div>
    </nav>
  );
  
}

export default Navbar;
