import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../css/DashboardStats.css'

const DashboardSection = () => {
  const [totalUsers, setTotalUsers] = useState(0);
  const [warehouseData, setWarehouseData] = useState([]);
  const [totalClients, setTotalClients] = useState(0);
  const role = localStorage.getItem('role');
  const loggedInUser = role === 'User' ? 'Client' : 'Admin';

  useEffect(() => {
    axios.get('https://localhost:44368/api/User')
      .then(response => {
        setTotalUsers(response.data.length);
      })
      .catch(error => {
        console.error('Error fetching users:', error);
      });

    axios.get('https://localhost:44368/api/Warehouse')
      .then(response => {
        setWarehouseData(response.data);
      })
      .catch(error => {
        console.error('Error fetching warehouses:', error);
      });

    axios.get('https://localhost:44368/api/CLient')
      .then(response => {
        setTotalClients(response.data.length);
      })
      .catch(error => {
        console.error('Error fetching clients:', error);
      });
  }, []);

  return (
    <div className="content-wrapper">
      <div className="dashboard-section" style={{ marginLeft: loggedInUser === 'Admin' ? '' : '0' }}>
        <div className="dashboard-card">
          <h3>Total Users</h3>
          <p>{totalUsers}</p>
        </div>
        {warehouseData
          .filter(warehouse => warehouse.name) // Filter out warehouses with no name
          .sort((a, b) => a.name.localeCompare(b.name)) // Sort warehouses by name
          .map(warehouse => (
            <div key={warehouse.id} className="dashboard-card">
              <h3>{warehouse.name}</h3>
              <p>Capacity: {warehouse.capacity}/7500 kg</p>
            </div>
          ))}

        <div className="dashboard-card">
          <h3>Total Clients</h3>
          <p>{totalClients}</p>
        </div>
      </div>
    </div>
  );
};

export default DashboardSection;
