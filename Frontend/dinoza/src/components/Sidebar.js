import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import axios from 'axios';
import '../css/Sidebar.css';
import LogoutButton from './LogoutButtonDashboard.js';

const Sidebar = () => {
  const [expandedItems, setExpandedItems] = useState({});
  const [unreadNotifications, setUnreadNotifications] = useState(0);
  const [data, setData] = useState([]);
  const location = useLocation();
  const handleToggle = (index) => {
    setExpandedItems((prevState) => ({
      ...prevState,
      [index]: !prevState[index],
    }));
  };

  
  useEffect(() => {
    // Fetch data using Axios
    axios.get('https://localhost:44368/api/Notification')
      .then(response => {
        setData(response.data);
        console.log(response.data);
        const unreadNotifications = response.data.filter(notification => !notification.isRead);
        setUnreadNotifications(unreadNotifications.length); 
        console.log(unreadNotifications);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  return (
    <div className="sidebar">
      <div className="logo">
        <Link to="/">
          <img src="/img/logo.png" alt="Company Logo" />
        </Link>
      </div>
      <LogoutButton/>
      <ul className="sidebar-menu">
        <li>
          <div className="dropdown-container">
            <div className="menu-item" >
              <Link to="/home">Dashboard</Link>
            </div>
          </div>
        </li>
        <li>
          <div className="dropdown-container">
          <div className="menu-item">
            <Link to="/notifications">
              Notifications {' '}
            </Link>
            {(unreadNotifications > 0 && location.pathname!="/notifications")&& (
                <span className="unread-count" style={{color: 'red', fontSize: '25px'}} >{unreadNotifications}</span>
              )}
          </div>
          </div>
        </li>
        <li>
          <div className="dropdown-container">
            <div className="menu-item" onClick={() => handleToggle(1)}>
              <Link to="">Orders</Link>
              <span className={`dropdown-arrow ${expandedItems[1] ? 'open' : ''}`}>&#9660;</span>
            </div>
            {expandedItems[1] && (
              <div className="dropdown-content">
                <Link to="/order/add">add order</Link>
                <Link to="/order/view">view orders</Link>
              </div>
            )}
          </div>
        </li>
        <li>
          <div className="dropdown-container">
            <div className="menu-item" onClick={() => handleToggle(3)}>
              <Link to="">Production</Link>
              <span className={`dropdown-arrow ${expandedItems[3] ? 'open' : ''}`}>&#9660;</span>
            </div>
            {expandedItems[3] && (
              <div className="dropdown-content">
                <Link to="/production/add">add production</Link>
                <Link to="/production/view">view productions</Link>
              </div>
            )}
          </div>
        </li>
        <li>
          <div className="dropdown-container">
            <div className="menu-item" onClick={() => handleToggle(2)}>
              <Link to="">Invoices</Link>
              <span className={`dropdown-arrow ${expandedItems[2] ? 'open' : ''}`}>&#9660;</span>
            </div>
            {expandedItems[2] && (
              <div className="dropdown-content">
                <Link to="/invoice/add">add invoice</Link>
                <Link to="/invoice/view">view invoices</Link>
              </div>
            )}
          </div>
        </li>
        
        <li>
          <div className="dropdown-container">
            <div className="menu-item" onClick={() => handleToggle(4)}>
              <Link to="">Clients</Link>
              <span className={`dropdown-arrow ${expandedItems[4] ? 'open' : ''}`}>&#9660;</span>
            </div>
            {expandedItems[4] && (
              <div className="dropdown-content">
                <Link to="/client/add">add clients</Link>
                <Link to="/client/view">view clients</Link>
              </div>
            )}
          </div>
        </li>
        <li>
          <div className="dropdown-container">
            <div className="menu-item" onClick={() => handleToggle(5)}>
              <Link to="">Suppliers</Link>
              <span className={`dropdown-arrow ${expandedItems[5] ? 'open' : ''}`}>&#9660;</span>
            </div>
            {expandedItems[5] && (
              <div className="dropdown-content">
                <Link to="/supplier/add">add suppliers</Link>
                <Link to="/supplier/view">view suppliers</Link>
              </div>
            )}
          </div>
        </li>
        <li>
          <div className="dropdown-container">
            <div className="menu-item" onClick={() => handleToggle(6)}>
              <Link to="">Users</Link>
              <span className={`dropdown-arrow ${expandedItems[6] ? 'open' : ''}`}>&#9660;</span>
            </div>
            {expandedItems[6] && (
              <div className="dropdown-content">
                <Link to="/user/view">view users</Link>
              </div>
            )}
          </div>
        </li>
      </ul>
    </div>
    
  );
};

export default Sidebar;
