import React from 'react';
import Sidebar from '../Sidebar';
import SupplierTable from './SupplierTable'; 
import '../../css/UserTable.css';

const ViewSuppliers = () => {
    const role = localStorage.getItem('role');
    const loggedInUser = role === 'User' ? 'Client' : 'Admin';
  
  return (
    <div className="view-user">
      <Sidebar />
      <div className="main-content" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
        <h2 className="endpoint-heading">supplier/view</h2>
        <hr></hr>
        <SupplierTable /> 
      </div>
    </div>
  );
};

export default ViewSuppliers;
