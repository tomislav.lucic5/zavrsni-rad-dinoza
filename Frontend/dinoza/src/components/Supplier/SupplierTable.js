import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../../css/UserTable.css';

const TableComponent = () => {

  const [data, setData] = useState([]);

  useEffect(() => {
    // Fetch data using Axios
    axios.get('https://localhost:44368/api/Supplier')
      .then(response => {
        setData(response.data);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  return (
    <div className="table-container" >
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>OIB</th>
            <th>Street</th>
            <th>City</th>
            <th>ZIP</th>
            <th>Country</th>
          </tr>
        </thead>
        <tbody>
          {data.map(client => (
            <tr key={client.id}>
              <td>{client.name}</td>
              <td>{client.oib}</td>
              <td>{client.street}</td>
              <td>{client.city}</td>
              <td>{client.zip}</td>
              <td>{client.country}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <p className="total-users">Total suppliers: {data.length}</p>
    </div>
  );
};

export default TableComponent;
