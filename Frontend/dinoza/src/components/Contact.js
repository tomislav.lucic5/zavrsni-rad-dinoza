import React from 'react';
import '../css/Contact.css';

function Contact() {
  return (
    <section className="contact">
      <h1 className="section-heading">How can you find us?</h1>
      <div className="contact-row">
        <div className="contact-section">
          <h3>Address</h3>
          <p>Savska ul. 94, 32275, Bošnjaci</p>
        </div>
        <div className="contact-section">
          <h3>Phone Number</h3>
          <p>+385-99-844-2573</p>
        </div>
        <div className="contact-section">
          <h3>Email</h3>
          <p>info@dinoza.hr</p>
        </div>
      </div>
    </section>
  );
}

export default Contact;
