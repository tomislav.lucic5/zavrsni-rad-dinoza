import React, { useState } from 'react';
import '../css/Home.css';
import axios from 'axios';
import { PDFDownloadLink } from "@react-pdf/renderer";
import YourPDFDocument from './PDFDocument';
import Sidebar from './Sidebar'; 
import DashboardStats from './DashboardStats'; 
import ChartComponent from './ChartComponent';
import Navbar from './Navbar';

function Home() {
  const [invoiceNumber, setInvoiceNumber] = useState('');
  const [invoiceInfo, setInvoiceInfo] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false); // Add loading state

  const role = localStorage.getItem('role');
  const loggedInUser = role === 'User' ? 'Client' : 'Admin';

  const handleSearch = async () => {
    try {
      setIsLoading(true); 
      await new Promise(resolve => setTimeout(resolve, 700));
      const response = await axios.get(
        `https://localhost:44368/api/Invoice?invoiceNumber=${invoiceNumber}`
      );
      setInvoiceInfo(response.data[0]);
      setErrorMessage('');
      
    } catch (error) {
      setInvoiceInfo(null);
      setErrorMessage('Invoice with that number does not exist.');
    } finally {
      setIsLoading(false); // Set loading to false after the API call is done
    }
  };

  return (
    
    <div className="dashboard">
      {loggedInUser === 'Admin' && <Sidebar />}
    <div className="dashboard-section" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
      {loggedInUser === 'Admin' && <DashboardStats />} 
      </div>
      <div className="dashboard-section" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
      {loggedInUser === 'Admin' && <ChartComponent />} 
      </div>
      <div className="home-container">
        {loggedInUser === 'Client' && (
          <div className="track-invoice">
            <h2>Track Invoice</h2>
            <input
              type="text"
              placeholder="Insert invoice number"
              className="search-bar"
              value={invoiceNumber}
              onChange={(e) => setInvoiceNumber(e.target.value)}
            />
            <button className="search-button" onClick={handleSearch}>
              Search
            </button>
          </div>
        )}
        {errorMessage && <p className="error-message">{errorMessage}</p>}
        {isLoading ? (
          <div className="loading-spinner">
            <div className="spinner"></div>
          </div>
        ) : (
          <>
            {invoiceInfo && (
              <div className="invoice-card">
                <h3>Invoice Information</h3>
                <hr />
                <h2>#{invoiceInfo.invoiceNumber}</h2>
                <p>Client Name: {invoiceInfo.clientName}</p>
                <p>Date Created: {new Date(invoiceInfo.dateCreated).toLocaleDateString()}</p>
                <p>
                  Invoice Creator: {invoiceInfo.invoiceCreatorName}{' '}
                  {invoiceInfo.invoiceCreatorLastName}
                </p>
                <p>Manufacturer Name: {invoiceInfo.manufacturerName}</p>
                <p>Price: {invoiceInfo.price}€</p>
                <div className="pdf-button-container">
                  <PDFDownloadLink
                    document={<YourPDFDocument invoiceInfo={invoiceInfo} />}
                    fileName={`invoice_${invoiceInfo.invoiceNumber}.pdf`}
                  >
                    <button className="pdf-button">Export to PDF</button>
                  </PDFDownloadLink>
                </div>
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );
}

export default Home;
