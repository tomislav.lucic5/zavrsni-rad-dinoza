import React from "react";
import { Page, Text, View, Document, StyleSheet, Image } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  page: {
    flexDirection: "column",
    padding: 20,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
  },
  logo: {
    width: 60,
    height: 60,
  },
  titleContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  title: {
    fontSize: 24,
  },
  info: {
    fontSize: 12,
    textAlign: "center",
    marginBottom: 10,
  },
  separator: {
    marginVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: "black",
  },
});

const YourPDFDocument = ({ invoiceInfo }) => (
  <Document>
    <Page size="A4" style={styles.page}>
      <View style={styles.header}>
        <Image style={styles.logo} src={"/img/logo.png"} />
        <Text style={styles.title}>Invoice #{invoiceInfo.invoiceNumber}</Text>
      </View>
      <View>
        <Text style={styles.info}>CLIENT: {invoiceInfo.clientName}</Text>
        <Text style={styles.info}>INVOICE DATE: {new Date(invoiceInfo.dateCreated).toLocaleDateString()}</Text>
        <Text style={styles.info}>
          INVOICE EXECUTIVE: {invoiceInfo.invoiceCreatorName} {invoiceInfo.invoiceCreatorLastName}
        </Text>
        <Text style={styles.info}>MANUFACTURER: {invoiceInfo.manufacturerName}</Text>
        <Text style={styles.info}>TOTAL PRICE: {invoiceInfo.price}</Text>
      </View>
      <View style={styles.separator} />
      <Text style={styles.info}>Thank you for your business!</Text>
    </Page>
  </Document>
);

export default YourPDFDocument;
