import React from 'react';
import '../css/Footer.css'; 

function Footer() {
  return (
    <footer className="footer">
      <div className="footer-container">
        <p className="footer-text">© 2023 Dinoza. All rights reserved.</p>
      </div>
    </footer>
  );
}

export default Footer;
