import React from 'react';
import '../css/Header.css'; // Import your CSS file for styling

function Header() {
  return (
    <header className="header">
      <h1 className="main-text">DINOZA</h1>
      <p className="sub-text">Dinoza is a company successful in production and selling of the salted raw skin.</p>
    </header>
  );
}

export default Header;
