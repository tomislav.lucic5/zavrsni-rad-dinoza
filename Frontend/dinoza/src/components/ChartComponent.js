import React, { useEffect, useState} from 'react';
import axios from 'axios';
import { BarChart, Bar, XAxis, YAxis, Legend, Tooltip, CartesianGrid } from 'recharts';
import '../css/ChartComponent.css'
import CalendarComponent from './CalendarComponent';

const ChartComponent = () => {
    const [data, setData] = useState([]);
    const [invoiceData, setInvoiceData] = useState([]);
    const [clientData, setClientData] = useState([]);
    const role = localStorage.getItem('role');
    const loggedInUser = role === 'User' ? 'Client' : 'Admin';
    const COLORS = ['#2085ec', '#8464a0', '#cea9bc' ];

  useEffect(() => {
    axios.get('https://localhost:44368/api/GoodsCalculation')
      .then(response => {
        const fetchedData = response.data;
  
        // Count the occurrences of each StatusName
        const statusCounts = fetchedData.reduce((counts, item) => {
          const statusName = item.statusName; // Use the correct field name
          counts[statusName] = (counts[statusName] || 0) + 1;
          return counts;
        }, {});
  
        // Prepare data for the bar chart
        const chartData = Object.keys(statusCounts).map(statusName => ({
          label: statusName,
          value: statusCounts[statusName],
        }));
  
        setData(chartData); // Set the chart data
        console.log(chartData);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });

      axios.get('https://localhost:44368/api/Invoice')
      .then(response => {
        setInvoiceData(response.data);
      })
      .catch(error => {
        console.error('Error fetching invoice data:', error);
      });

    axios.get('https://localhost:44368/api/Supplier')
      .then(response => {
        setClientData(response.data);
      })
      .catch(error => {
        console.error('Error fetching client data:', error);
      });
  }, []);
  return (
    <div className="content-wrapper">
    <div className="chart-container" style={{ marginLeft: loggedInUser === 'Admin' ? 'auto' : '0' }}>
      <div className="chart-row">
        <div className="chart">
          <h2>WORK ORDERS</h2>
          <BarChart width={400} height={300} data={data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="label" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="value" fill="#8884d8" />
          </BarChart>
        </div>
      </div>

      <div className="chart-row">
        <div className="chart" style={{padding:30}}>
        <h2>CALENDAR</h2>
        <CalendarComponent /> {/* Use the CalendarComponent */}
        </div>
      </div>
    </div>
    </div>
  );
};

export default ChartComponent;
