import React from 'react';

export default function MissingPage() {
  return (
    <div style={styles.missingPage}>
      <h1 style={styles.heading}>Oops! This page doesn't exist.</h1>
      <p style={styles.paragraph}>
        Sorry, but the page you are looking for could not be found.
      </p>
    </div>
  );
}

const styles = {
  missingPage: {
    backgroundColor: 'transparent',
    padding: '40px',
    textAlign: 'center',
    fontFamily: 'Arial, sans-serif',
  },
  heading: {
    fontSize: '80px',
    color: 'white',
    marginBottom: '40px',
  },
  paragraph: {
    fontSize: '35px',
    color: 'white',
  },
};
