import React, { useState, useEffect } from 'react';
import axios from 'axios';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Sidebar from '../Sidebar.js';

const AddDataForm = () => {
  const [formData, setFormData] = useState({
    dateOfReceipt: '',
    paymentDeadline: '',
    name: '',
    price: '',
    goodsClass: '',
    jm: 'KG',
    amount: '',
    orderNumber: '',
    warehouse: '',
    supplierId: '',
    statusId: '',
  });

  const [statuses, setStatuses] = useState([]);
  const [suppliers, setSuppliers] = useState([]);
  const [warehouses, setWarehouses] = useState([]);

  const warehouseIdMap = {
    1: '7566cd6f-4afe-4e7c-8f23-914155b627e5',
    2: '647008ce-3f87-4a7a-ab1f-9e9b241b405f',
    3: 'bc2f6f40-ecea-4c5f-b18a-c4e721df772d',
  };

  useEffect(() => {
    axios.get('https://localhost:44368/api/Supplier')
      .then(response => {
        setSuppliers(response.data);
      })
      .catch(error => {
        console.error('Error fetching suppliers:', error);
      });

    axios.get('https://localhost:44368/api/Status')
      .then(response => {
        setStatuses(response.data);
      })
      .catch(error => {
        console.error('Error fetching statuses:', error);
      });

    axios.get('https://localhost:44368/api/Warehouse')
      .then(response => {
        setWarehouses(response.data);
      })
      .catch(error => {
        console.error('Error fetching warehouses:', error);
      });

  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleUpdateWarehouseCapacity = (warehouseId, capacityChange) => {
    axios.put(`https://localhost:44368/api/Warehouse/${warehouseId}?capacity=${capacityChange}`)
      .then((response) => {
        console.log('Warehouse capacity updated successfully', response.data);
      })
      .catch((error) => {
        console.error('Error updating warehouse capacity', error);
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const warehouseId = warehouseIdMap[formData.warehouse];
    const selectedWarehouse = warehouses.find(warehouse => warehouse.id === warehouseId);
    console.log(selectedWarehouse);
    if (7500 - selectedWarehouse.capacity < parseInt(formData.amount)) {
      alert('Warehouse capacity is insufficient for the amount provided.');
      return;
    }

    axios.post('https://localhost:44368/api/GoodsCalculation', formData)
      .then((response) => {
        console.log('Data added successfully', response.data);

        setFormData({
          dateOfReceipt: '',
          paymentDeadline: '',
          name: '',
          price: '',
          goodsClass: '',
          jm: 'KG',
          amount: '',
          orderNumber: '',
          warehouse: '',
          supplierId: '',
          statusId: '',
        });

        const warehouseId = warehouseIdMap[formData.warehouse];
        const capacityChange = parseInt(formData.amount);

        handleUpdateWarehouseCapacity(warehouseId, capacityChange);

      })
      .catch((error) => {
        console.error('Error adding data', error);
      });
  };

  return (
    <div className="add-data-form">
      <Sidebar />
      <h2>ADD GOODS CALCULATION</h2>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Order Number"
          name="orderNumber"
          value={formData.orderNumber}
          onChange={handleChange}
          required
        />
        <FormControl>
          <InputLabel>Supplier</InputLabel>
          <Select
            label="Supplier"
            name="supplierId"
            value={formData.supplierId}
            onChange={handleChange}
            required
          >
            {suppliers.map(supplier => (
              <MenuItem key={supplier.id} value={supplier.id}>
                {supplier.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <TextField
            label="Goods Name"
            name="name"
            value={formData.name}
            onChange={handleChange}
            required
          />
          <TextField
            label="Price"
            name="price"
            value={formData.price}
            onChange={handleChange}
            required
            type="number" // Set the input type to number
            inputProps={{
                min: 0, // Minimum value allowed
                step: 0.01, // Specify the step for decimal numbers
            }}
            />

        <TextField
          label="Amount"
          name="amount"
          value={formData.amount}
          onChange={handleChange}
          required
          type="number"
        />
          <TextField
          label="jm"
          name="jm"
          value={formData.jm}
          disabled // Set to true to disable the input
        />
        <TextField
          label="Goods Class"
          name="goodsClass"
          value={formData.goodsClass}
          onChange={handleChange}
          required
        />
        <TextField
          label="Warehouse"
          name="warehouse"
          value={formData.warehouse}
          onChange={handleChange}
          required
          type="number"
          inputProps={{
            min: 1,
            max: 3, // Assuming the maximum value is 3, adjust it according to your needs
          }}
        />

<FormControl>
  <InputLabel>Status</InputLabel>
  <Select
    label="Status"
    name="statusId"
    value={formData.statusId}
    onChange={handleChange}
    required
  >
    {statuses.map(status => (
      <MenuItem
        key={status.id}
        value={status.id}
        disabled={status.name === "Shipping"}
      >
        {status.name}
      </MenuItem>
    ))}
  </Select>
</FormControl>

        <Button variant="contained" color="primary" type="submit">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default AddDataForm;
