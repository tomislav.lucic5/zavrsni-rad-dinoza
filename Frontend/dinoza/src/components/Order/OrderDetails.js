import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import '../../css/OrderDetails.css'; // Import your custom CSS file
import Sidebar from '../Sidebar.js';

const OrderDetails = () => {
  const { id } = useParams();
  const [orderDetails, setOrderDetails] = useState(null);
  const [editableStatus, setEditableStatus] = useState(false);
  const [newStatus, setNewStatus] = useState('');
  const [statuses, setStatuses] = useState([]);

  useEffect(() => {
    // Fetch statuses and warehouses from API
    axios.get('https://localhost:44368/api/Status')
      .then(response => {
        setStatuses(response.data);
      })
      .catch(error => {
        console.error('Error fetching statuses:', error);
      });

    // Fetch order details
    axios.get(`https://localhost:44368/api/GoodsCalculation/${id}`)
      .then(response => {
        setOrderDetails(response.data[0]);
        setNewStatus(response.data[0].statusName);
      })
      .catch(error => {
        console.error('Error fetching order details:', error);
      });
  }, [id]);

  const handleStatusChange = () => {
    const selectedStatus = statuses.find(status => status.name === newStatus);
  
    if (!selectedStatus) {
      console.error('Selected status not found');
      return;
    }
  
    // Send an API request to update the status
    axios.put(`https://localhost:44368/api/GoodsCalculation/${id}`, {
      statusId: selectedStatus.id,
    })
      .then(response => {
        // Update the orderDetails with the updated status
        setOrderDetails({
          ...orderDetails,
          statusName: newStatus
        });
        setEditableStatus(false);
  
        // Check if the selected status is "Shipping"
        if (selectedStatus.name === "Shipping") {
          const warehouseIdMap = {
            1: '7566cd6f-4afe-4e7c-8f23-914155b627e5',
            2: '647008ce-3f87-4a7a-ab1f-9e9b241b405f',
            3: 'bc2f6f40-ecea-4c5f-b18a-c4e721df772d',
          };
          const warehouseId = warehouseIdMap[orderDetails.warehouse]; 
          const capacityChange = parseInt(orderDetails.amount);
  
          axios.put(`https://localhost:44368/api/Warehouse/${warehouseId}/removecapacity?capacity=${capacityChange}`)
            .then(response => {
              console.log('Warehouse capacity removed successfully', response.data);
            })
            .catch(error => {
              console.error('Error removing warehouse capacity', error);
            });
        }
      })
      .catch(error => {
        console.error('Error updating status:', error);
      });
  };

  if (!orderDetails) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <div className="order-card">
        <Sidebar />
        <h2>#{orderDetails.orderNumber}</h2>
        <hr />
        <div className="order-info">
          <div className="info-row">
            <p className="info-label">Order Date:</p>
            <p className="info-value">{new Date(orderDetails.dateOfReceipt).toLocaleDateString()}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Payment Deadline:</p>
            <p className="info-value">{new Date(orderDetails.paymentDeadline).toLocaleDateString()}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Total Payment:</p>
            <p className="info-value">{orderDetails.amount * orderDetails.price}€</p>
          </div>
          <div className="info-row">
            <p className="info-label">Order Status:</p>
            <p className="info-value">
              {editableStatus ? (
                <>
                  <select
                    value={newStatus}
                    onChange={(e) => setNewStatus(e.target.value)}
                  >
                    {statuses.map(status => (
                      <option key={status.id} value={status.name}>
                        {status.name}
                      </option>
                    ))}
                  </select>
                  <button onClick={handleStatusChange}>Save</button>
                </>
              ) : (
                <>
                  {orderDetails.statusName}
                  <button onClick={() => setEditableStatus(true)}>Edit</button>
                </>
              )}
            </p>
          </div>
          <hr />
          <div className="info-row">
            <p className="info-label">Supplier:</p>
            <p className="info-value">{orderDetails.supplierName}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Goods Name:</p>
            <p className="info-value">{orderDetails.name}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Goods Class:</p>
            <p className="info-value">{orderDetails.goodsClass}</p>
          </div>
          <div className="info-row">
            <p className="info-label">JM:</p>
            <p className="info-value">{orderDetails.jm}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Amount:</p>
            <p className="info-value">{orderDetails.amount}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Warehouse:</p>
            <p className="info-value">{orderDetails.warehouse}</p>
          </div>
        </div>
      </div>
    </div>
  );
  
};

export default OrderDetails;
