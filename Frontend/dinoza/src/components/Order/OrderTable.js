// import React, { useEffect, useState } from 'react';
// import axios from 'axios';
// import { Link } from 'react-router-dom';
// import '../../css/UserTable.css';

// const TableComponent = () => {
//      const [searchQuery, setSearchQuery] = useState('');
//      const [data, setData] = useState([]);

//   useEffect(() => {
//     // Fetch data using Axios
//     axios.get('https://localhost:44368/api/GoodsCalculation')
//       .then(response => {
//         setData(response.data);
//         console.log(response.data);
//       })
//       .catch(error => {
//         console.error('Error fetching data:', error);
//       });
//   }, []);
//   const filteredData = data.filter(goodsCalculation =>
//     goodsCalculation.orderNumber.toString().includes(searchQuery)
//   );
  
//   return (
    
//     <div className="table-container">
//         <div className="search-container">
//         <input
//             className="search-input"
//             type="text"
//             placeholder="Search by Order Number"
//             value={searchQuery}
//             onChange={(e) => setSearchQuery(e.target.value)}
//         />
//         </div>
//   <table className="table">
//     <thead>
//       <tr>
//         <th>Order Number</th>
//         <th>Date Created</th>
//         <th>Payment Deadline</th>
//         <th>Goods Name</th>
//         <th>Goods Class</th>
//         <th>JM</th>
//         <th>Amount</th>
//         <th>Price</th>
//         <th>total payment</th>
//         <th>Warehouse</th>
//         <th>Order Status</th>
//         <th>Goods Supplier</th>
//       </tr>
//     </thead>
//     <tbody>
//       {filteredData.map(goodsCalculation => (
//         <tr key={goodsCalculation.id}>
//           <td>
//                 <Link to={`/order/view/${goodsCalculation.id}`} className="no-decoration-link">
//                   #{goodsCalculation.orderNumber}
//                 </Link>
//           </td>
//           <td>{new Date(goodsCalculation.dateOfReceipt).toLocaleDateString()}</td>
//           <td>{new Date(goodsCalculation.paymentDeadline).toLocaleDateString()}</td>
//           <td>{goodsCalculation.name}</td>
//           <td>{goodsCalculation.goodsClass}</td>
//           <td>{goodsCalculation.jm}</td>
//           <td>{goodsCalculation.amount}</td>
//           <td>{goodsCalculation.price}</td>
//           <td>{goodsCalculation.amount * goodsCalculation.price}€</td>
//           <td>{goodsCalculation.warehouse}</td>
//           <td>{goodsCalculation.statusName}</td>
//           <td>{goodsCalculation.supplierName}</td>
//         </tr>
//       ))}
//     </tbody>
//   </table>
//   <p className="total-users">Total Orders: {data.length}</p>
// </div>

//   );
// };

// export default TableComponent;
