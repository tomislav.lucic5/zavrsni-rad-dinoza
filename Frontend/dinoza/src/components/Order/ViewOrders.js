import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Sidebar from '../Sidebar';
import '../../css/UserTable.css';
import { Link } from 'react-router-dom';

const ViewOrders = () => {
  const role = localStorage.getItem('role');
  const loggedInUser = role === 'User' ? 'Client' : 'Admin';
  const [searchQuery, setSearchQuery] = useState('');
  const [data, setData] = useState([]);

  useEffect(() => {
    axios.get('https://localhost:44368/api/GoodsCalculation')
      .then(response => {
        setData(response.data);
        console.log(response.data);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  const filteredData = data.filter(goodsCalculation =>
    goodsCalculation.orderNumber.toString().includes(searchQuery)
  );

  return (
    <div className="view-user">
      <Sidebar />
      <div className="main-content" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
        <h2 className="endpoint-heading">order/view</h2>
        <hr />
        <div className="table-container">
          <div className="search-container">
            <input
              className="search-input"
              type="text"
              placeholder="Search by Order Number"
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
            />
          </div>
          <table className="table">
            <thead>
              <tr>
                <th>Order Number</th>
                <th>Date Created</th>
                <th>Payment Deadline</th>
                <th>Goods Name</th>
                <th>Goods Class</th>
                <th>JM</th>
                <th>Amount</th>
                <th>Price</th>
                <th>total payment</th>
                <th>Warehouse</th>
                <th>Order Status</th>
                <th>Goods Supplier</th>
              </tr>
            </thead>
            <tbody>
              {filteredData.map(goodsCalculation => (
                <tr key={goodsCalculation.id}>
                  <td>
                    <Link to={`/order/view/${goodsCalculation.id}`} className="no-decoration-link">
                      #{goodsCalculation.orderNumber}
                    </Link>
                  </td>
                  <td>{new Date(goodsCalculation.dateOfReceipt).toLocaleDateString()}</td>
                  <td>{new Date(goodsCalculation.paymentDeadline).toLocaleDateString()}</td>
                  <td>{goodsCalculation.name}</td>
                  <td>{goodsCalculation.goodsClass}</td>
                  <td>{goodsCalculation.jm}</td>
                  <td>{goodsCalculation.amount}</td>
                  <td>{goodsCalculation.price}</td>
                  <td>{goodsCalculation.amount * goodsCalculation.price}€</td>
                  <td>{goodsCalculation.warehouse}</td>
                  <td>{goodsCalculation.statusName}</td>
                  <td>{goodsCalculation.supplierName}</td>
                </tr>
              ))}
            </tbody>
          </table>
          <p className="total-users">Total Orders: {data.length}</p>
        </div>
      </div>
    </div>
  );
};

export default ViewOrders;
