import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import '../../css/OrderDetails.css'; // Import your custom CSS file
import Sidebar from '../Sidebar.js';

const ProductionDetails = () => {
  const { id } = useParams();
  const [productionDetails, setProductionDetails] = useState(null);
  const [editableStatus, setEditableStatus] = useState(false);
  const [newStatus, setNewStatus] = useState('');
  const [statuses, setStatuses] = useState([]);

  useEffect(() => {
    // Fetch statuses and warehouses from API
    axios.get('https://localhost:44368/api/Status')
      .then(response => {
        setStatuses(response.data);
      })
      .catch(error => {
        console.error('Error fetching statuses:', error);
      });

    // Fetch order details
    axios.get(`https://localhost:44368/api/MaterialProduction/${id}`)
      .then(response => {
        setProductionDetails(response.data[0]);
        setNewStatus(response.data[0].statusName);
      })
      .catch(error => {
        console.error('Error fetching order details:', error);
      });
  }, [id]);

  const handleStatusChange = () => {
    const selectedStatus = statuses.find(status => status.name === newStatus);

    if (!selectedStatus) {
      console.error('Selected status not found');
      return;
    }

    // Send an API request to update the status
    axios.put(`https://localhost:44368/api/MaterialProduction/${id}`, {
      statusId: selectedStatus.id,
    })
      .then(response => {
        // Update the orderDetails with the updated status
        setProductionDetails({
          ...productionDetails,
          statusName: newStatus
        });
        setEditableStatus(false);
      })
      .catch(error => {
        console.error('Error updating status:', error);
      });
  };

  if (!productionDetails) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <div className="order-card">
        <Sidebar />
        <h2>#{productionDetails.productionNumber}</h2>
        <hr />
        <div className="order-info">
          <div className="info-row">
            <p className="info-label">Order Number:</p>
            <p className="info-value">#{productionDetails.orderNumber}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Production Date:</p>
            <p className="info-value">{new Date(productionDetails.dateCreated).toLocaleDateString()}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Resource Price:</p>
            <p className="info-value">{productionDetails.resourcePrice}€</p>
          </div>
          <div className="info-row">
            <p className="info-label">Production Status:</p>
            <p className="info-value">
              {editableStatus ? (
                <>
                  <select
                    value={newStatus}
                    onChange={(e) => setNewStatus(e.target.value)}
                  >
                    {statuses.map(status => (
                      <option key={status.id} value={status.name}>
                        {status.name}
                      </option>
                    ))}
                  </select>
                  <button onClick={handleStatusChange}>Save</button>
                </>
              ) : (
                <>
                  {productionDetails.statusName}
                  <button onClick={() => setEditableStatus(true)}>Edit</button>
                </>
              )}
            </p>
          </div>
          <hr />
          <div className="info-row">
            <p className="info-label">Material:</p>
            <p className="info-value">{productionDetails.materialName}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Warehouse:</p>
            <p className="info-value">{productionDetails.warehouse}</p>
          </div>
          <div className="info-row">
            <p className="info-label">Goods Name:</p>
            <p className="info-value">{productionDetails.goodsName}</p>
          </div>
        </div>
      </div>
    </div>
  );
  
};

export default ProductionDetails;
