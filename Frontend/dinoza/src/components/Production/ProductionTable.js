import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../../css/UserTable.css';

const TableComponent = () => {
     const [searchQuery, setSearchQuery] = useState('');
     const [data, setData] = useState([]);

  useEffect(() => {
    // Fetch data using Axios
    axios.get('https://localhost:44368/api/MaterialProduction')
      .then(response => {
        setData(response.data);
        console.log(response.data);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);
  const filteredData = data.filter(materialProduction =>
    materialProduction.productionNumber.toString().includes(searchQuery)
  );
  
  return (
    <div className="table-container">
        <div className="search-container">
        <input
            className="search-input"
            type="text"
            placeholder="Search by Production Number"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
        />
        </div>
  <table className="table">
    <thead>
      <tr>
        <th>Production Number</th>
        <th>Date Created</th>
        <th>Order Number</th>
        <th>Material</th>
        <th>Goods Name</th>
        <th>Warehouse</th>
        <th>Resource Price</th>
        <th>Production Status</th>
      </tr>
    </thead>
    <tbody>
      {filteredData.map(materialProduction => (
        <tr key={materialProduction.id}>
          <td>
                <Link to={`/production/view/${materialProduction.id}`} className="no-decoration-link">
                  #{materialProduction.productionNumber}
                </Link>
          </td>
          <td>{new Date(materialProduction.dateCreated).toLocaleDateString()}</td>
          <td>{materialProduction.orderNumber}</td>
          <td>{materialProduction.materialName}</td>
          <td>{materialProduction.goodsName}</td>
          <td>{materialProduction.warehouse}</td>
          <td>{materialProduction.resourcePrice}</td>
          <td>{materialProduction.statusName}</td>
        </tr>
      ))}
    </tbody>
  </table>
  <p className="total-users">Total Productions: {data.length}</p>
</div>

  );
};

export default TableComponent;
