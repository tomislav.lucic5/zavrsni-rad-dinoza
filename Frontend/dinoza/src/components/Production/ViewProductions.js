import React from 'react';
import Sidebar from '../Sidebar';
import ProductionTable from './ProductionTable'; 
import '../../css/UserTable.css';

const ViewProductions = () => {
    const role = localStorage.getItem('role');
    const loggedInUser = role === 'User' ? 'Client' : 'Admin';
  
  return (
    <div className="view-user">
      <Sidebar />
      <div className="main-content" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
        <h2 className="endpoint-heading">production/view</h2>
        <hr></hr>
        <ProductionTable /> 
      </div>
    </div>
  );
};

export default ViewProductions;
