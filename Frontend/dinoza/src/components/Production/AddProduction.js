import React, { useState, useEffect } from 'react';
import axios from 'axios';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import '../../css/AddDataForm.css'; // Add your custom CSS for styling
import Sidebar from '../Sidebar.js';

const AddDataForm = () => {
  const [formData, setFormData] = useState({
    productionNumber: '',
    goodsCalculationId: '',
    materialId: '',
    resourcePrice: '',
    statusId: '',
  });

  const [materials, setMaterials] = useState([]);
  const [statuses, setStatuses] = useState([]);
  const [goodsCalculations, setGoodsCalculations] = useState([]);


  useEffect(() => {
    axios.get('https://localhost:44368/api/Material')
      .then(response => {
        setMaterials(response.data);
      })
      .catch(error => {
        console.error('Error fetching clients:', error);
      });

      axios.get('https://localhost:44368/api/Status')
      .then(response => {
        setStatuses(response.data);
      })
      .catch(error => {
        console.error('Error fetching statuses:', error);
      });

      axios.get('https://localhost:44368/api/GoodsCalculation')
      .then(response => {
        setGoodsCalculations(response.data);
      })
      .catch(error => {
        console.error('Error fetching statuses:', error);
      });

  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    axios.post('https://localhost:44368/api/MaterialProduction', formData)
      .then((response) => {
        console.log('Data added successfully', response.data);
        setFormData({
            productionNumber: '',
            goodsCalculationId: '',
            materialId: '',
            resourcePrice: '',
            statusId: '',
        });
      })
      .catch((error) => {
        console.error('Error adding data', error);
      });
  };

  return (
    <div className="add-data-form">
      <Sidebar />
      <h2>ADD PRODUCTION FORM</h2>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Production Number"
          name="productionNumber"
          value={formData.productionNumber}
          onChange={handleChange}
          required
        />
        <FormControl>
          <InputLabel>Order Number</InputLabel>
          <Select
            label="Order"
            name="goodsCalculationId"
            value={formData.goodsCalculationId}
            onChange={handleChange}
            required
          >
            {goodsCalculations.map(goodsCalculation => (
              <MenuItem key={goodsCalculation.id} value={goodsCalculation.id}>
                {goodsCalculation.orderNumber}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl>
          <InputLabel>Material</InputLabel>
          <Select
            label="Material"
            name="materialId"
            value={formData.materialId}
            onChange={handleChange}
            required
          >
            {materials.map(material => (
              <MenuItem key={material.id} value={material.id}>
                {material.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
          <TextField
            label="Resource Price"
            name="resourcePrice"
            value={formData.resourcePrice}
            onChange={handleChange}
            required
            type="number" 
            inputProps={{
                min: 0, 
                step: 0.01, 
            }}
            />
        <FormControl>
          <InputLabel>Status</InputLabel>
          <Select
            label="Status"
            name="statusId"
            value={formData.statusId}
            onChange={handleChange}
            required
          >
            {statuses.map(status => (
              <MenuItem key={status.id} value={status.id}>
                {status.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button variant="contained" color="primary" type="submit">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default AddDataForm;
