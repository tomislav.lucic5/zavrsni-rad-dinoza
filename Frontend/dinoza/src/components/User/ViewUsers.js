import React from 'react';
import Sidebar from '../Sidebar';
import UserTable from './UserTable'; 
import '../../css/UserTable.css';

const ViewUser = () => {
    const role = localStorage.getItem('role');
    const loggedInUser = role === 'User' ? 'Client' : 'Admin';
  
  return (
    <div className="view-user">
      <Sidebar />
      <div className="main-content" style={{ marginLeft: loggedInUser === 'Admin' ? '250px' : '0' }}>
        <h2 className="endpoint-heading">user/view</h2>
        <hr></hr>
        <UserTable /> {/* Render your UserTable component here */}
      </div>
    </div>
  );
};

export default ViewUser;
