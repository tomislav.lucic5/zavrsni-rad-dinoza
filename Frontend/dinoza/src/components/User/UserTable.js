import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../../css/UserTable.css';

const TableComponent = () => {

  const [data, setData] = useState([]);

  useEffect(() => {
    // Fetch data using Axios
    axios.get('https://localhost:44368/api/User')
      .then(response => {
        setData(response.data);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  return (
    <div className="table-container" >
      <table className="table">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          {data.map(user => (
            <tr key={user.id}>
              <td>{user.firstName}</td>
              <td>{user.lastName}</td>
              <td>{user.email}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <p className="total-users">Total users: {data.length}</p>
    </div>
  );
};

export default TableComponent;
