import React from 'react';
import '../css/Gallery.css'; // Import your CSS file for styling

const images = [
  'img/dinoza1.jpg',
  'img/dinoza2.jpg',
  'img/dinoza3.jpg',
  'img/dinoza4.jpg',
  'img/dinoza5.jpg',
  'img/dinoza6.jpg',
];

function GalleryItem({ src, alt }) {
  return (
    <div className="gallery-item">
      <img src={src} alt={alt} className="gallery-image" />
    </div>
  );
}

function Gallery() {
  return (
    <section className="gallery-section">
      <div className="gallery-row">
        {images.slice(0, 3).map((image, index) => (
          <GalleryItem key={index} src={image} alt={`Image ${index + 1}`} />
        ))}
      </div>
      <div className="gallery-row">
        {images.slice(3).map((image, index) => (
          <GalleryItem key={index} src={image} alt={`Image ${index + 4}`} />
        ))}
      </div>
    </section>
  );
}

export default Gallery;
