import React from 'react';
import '../css/AboutUs.css'; // Import your CSS file for styling

function AboutUsSection() {
  return (
    <div className="about-us-section">
      <div className="about-us-subsection">
        <h2>Dinoza - Bošnjaci</h2>
        <p>The company Dinoza d.o.o. (Ltd.) Bošnjaci was found in 2009 as a daughter-company whose 90% owner is Dinoza d.o.o. (Ltd.) Orašje.</p>
      </div>
      <div className="about-us-subsection">
        <h2>Dinoza - Orašje</h2>
        <p>The company Dinoza d.o.o (Ltd.) Orašje was found in 1990. Since then, it has been successful in production and selling of the salted raw skin.
</p>
      </div>
      <div className="about-us-subsection">
        <h2>What do we do?</h2>
        <p>The primary activity of Dinoza d.o.o (Ltd.) is the purchase of raw and salty skin, production and preserving of the skin and the salted raw skin wholesale. The company works mostly with beefs, neat’s, calfs, lambs, sheeps, goats and goatkid skin as well as the third category of neat’s skin.</p>
      </div>
    </div>
  );
}

export default AboutUsSection;
