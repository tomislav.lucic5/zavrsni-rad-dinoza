﻿using Dinoza.Model;
using Dinoza.Service.Common;
using Dinoza.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.WebApi.Controllers
{
    public class ClientController : ApiController
    {
        private readonly IClientService ClientService;
        public ClientController(IClientService ClientService)
        {
            this.ClientService = ClientService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllClientsAsync()
        {
            List<Client> Clients = new List<Client>();
            List<ClientRest> ClientsRest = new List<ClientRest>();
            try
            {
                Clients = await ClientService.GetClientsAsync();
                ClientsRest = SetModelToRest(Clients);
                return Request.CreateResponse(HttpStatusCode.OK, ClientsRest);
            }
            catch (Exception ex)
            {

                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
        [HttpPost]
        public async Task<HttpResponseMessage> AddClientAsync([FromBody] ClientRest clientRest)
        {
            Client client = new Client();
            client = SetModelFromRest(clientRest);
            bool result = await ClientService.AddClientAsync(client);
            if(result)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GetSpecificClient(Guid id)
        {
            Client Client = new Client();
            ClientRest ClientRest = new ClientRest();
            try
            {
                Client = await ClientService.GetSpecificClientAsync(id);
                List<Client> Clients = new List<Client>{
                    Client
                };
                List<ClientRest> ClientsRest = new List<ClientRest>();

                ClientsRest = SetModelToRest(Clients);
                return Request.CreateResponse(HttpStatusCode.OK, ClientsRest);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }


        [HttpPut]
        public async Task<HttpResponseMessage> UpdateClientAsync(Guid id, [FromBody] ClientRest ClientRest)
        {
            Client Client = new Client();
            int rowsAffected;
            try
            {
                Client = SetModelFromRest(ClientRest);
                rowsAffected = await ClientService.UpdateClientAsync(id, Client);
                return Request.CreateResponse(HttpStatusCode.OK, rowsAffected);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> DeleteClientAsync(Guid id)
        {
            int rowsAffected;
            try
            {
                rowsAffected = await ClientService.DeleteClientAsync(id);
                return Request.CreateResponse(HttpStatusCode.OK, rowsAffected);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        private List<ClientRest> SetModelToRest(List<Client> Clients)
        {
            List<ClientRest> restClients = new List<ClientRest>();

            foreach (Client Client in Clients)
            {

                ClientRest ClientRest = new ClientRest();
                ClientRest.Id = Client.Id;
                ClientRest.Name = Client.Name;
                ClientRest.PaymentType = Client.PaymentType;
                ClientRest.Street = Client.Address.Street;
                ClientRest.City = Client.Address.City;
                ClientRest.Country = Client.Address.Country;
                ClientRest.Zip = Client.Address.Zip;
                ClientRest.OIB = Client.OIB;
                restClients.Add(ClientRest);
            }
            return restClients;

        }
        private Client SetModelFromRest(ClientRest ClientRest)
        {
            Client client = new Client();
            client.Id = Guid.NewGuid();
            client.OIB = ClientRest.OIB;
            client.Name = ClientRest.Name;
            client.Address = new Address();

            client.Address.Street = ClientRest.Street;
            client.Address.City = ClientRest.City;
            client.Address.Country = ClientRest.Country;
            client.Address.Zip = ClientRest.Zip;
            client.PaymentType = ClientRest.PaymentType;


            return client;
        }
    }
}