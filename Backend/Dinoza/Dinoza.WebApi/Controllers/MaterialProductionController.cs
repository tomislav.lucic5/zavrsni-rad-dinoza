﻿using Dinoza.Model;
using Dinoza.Service.Common;
using Dinoza.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Dinoza.WebApi.Controllers
{
    public class MaterialProductionController : ApiController
    {
        private readonly IMaterialProductionService materialProductionService;
        public MaterialProductionController(IMaterialProductionService materialProductionService)
        {
            this.materialProductionService = materialProductionService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllMaterialProductionsAsync()
        {
            List<MaterialProduction> materialProductions = new List<MaterialProduction>();
            List<MaterialProductionRest> materialProductionsRest = new List<MaterialProductionRest>();
            try
            {
                materialProductions = await materialProductionService.GetMaterialProductionsAsync();
                materialProductionsRest = SetModelToRest(materialProductions);
                return Request.CreateResponse(HttpStatusCode.OK, materialProductionsRest);
            }
            catch (Exception ex)
            {

                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
        [HttpPost]
        public async Task<HttpResponseMessage> AddMaterialProductionAsync([FromBody] MaterialProductionRest materialProductionRest)
        {
            MaterialProduction materialProduction = new MaterialProduction();
            materialProduction = SetModelFromRest(materialProductionRest);
            bool result = await materialProductionService.AddMaterialProductionAsync(materialProduction);
            if (result)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GetSpecificInvoice(Guid id)
        {
            MaterialProduction materialProduction = new MaterialProduction();
            MaterialProductionRest materialProductionRest = new MaterialProductionRest();
            try
            {
                materialProduction = await materialProductionService.GetSpecificMaterialProductionAsync(id);
                List<MaterialProduction> materialProductions = new List<MaterialProduction>{
                    materialProduction
                };
                List<MaterialProductionRest> materialProductionRests = new List<MaterialProductionRest>();

                materialProductionRests = SetModelToRest(materialProductions);
                return Request.CreateResponse(HttpStatusCode.OK, materialProductions);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut]
        public async Task<HttpResponseMessage> UpdateMaterialProduction (Guid id, [FromBody] UpdateGoodsCalculationModel updateMaterialProduction)
        {
            int result;
            try
            {
                result = await materialProductionService.UpdateMaterialProductionStatusAsnyc(id, updateMaterialProduction);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        private List<MaterialProductionRest> SetModelToRest(List<MaterialProduction> materialProductions)
        {
            List <MaterialProductionRest> materialProductionRests = new List<MaterialProductionRest>();

            foreach (MaterialProduction materialProduction in materialProductions)
            {

                MaterialProductionRest materialProductionRest = new MaterialProductionRest();
                materialProductionRest.Id = materialProduction.Id;
                materialProductionRest.DateCreated = materialProduction.DateCreated;
                materialProductionRest.Warehouse = materialProduction.Warehouse;
                materialProductionRest.StatusName = materialProduction.StatusName;
                materialProductionRest.ResourcePrice = materialProduction.ResourcePrice;
                materialProductionRest.GoodsName = materialProduction.GoodsName;
                materialProductionRest.ProductionNumber = materialProduction.ProductionNumber;
                materialProductionRest.MaterialName = materialProduction.MaterialName;
                materialProductionRest.OrderNumber = materialProduction.OrderNumber;
                materialProductionRests.Add(materialProductionRest);

            }
            return materialProductionRests;

        }
        private MaterialProduction SetModelFromRest(MaterialProductionRest materialProductionRest)
        {
            MaterialProduction materialProduction = new MaterialProduction();
            materialProduction.Id = Guid.NewGuid();
            materialProduction.DateCreated = DateTime.Now;
            materialProduction.Warehouse = materialProductionRest.Warehouse;
            materialProduction.StatusName = materialProductionRest.StatusName;
            materialProduction.ResourcePrice = materialProductionRest.ResourcePrice;
            materialProduction.GoodsCalculationId = materialProductionRest.GoodsCalculationId;
            materialProduction.MaterialId = materialProductionRest.MaterialId;
            materialProduction.GoodsName = materialProductionRest.GoodsName;
            materialProduction.ProductionNumber = materialProductionRest.ProductionNumber;
            materialProduction.MaterialName = materialProductionRest.MaterialName;
            materialProduction.StatusId = materialProductionRest.StatusId;
            materialProduction.OrderNumber = materialProductionRest.OrderNumber;

            return materialProduction;
        }
    }
}