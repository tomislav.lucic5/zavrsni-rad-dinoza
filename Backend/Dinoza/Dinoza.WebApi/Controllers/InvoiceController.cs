﻿using Dinoza.Model;
using Dinoza.Service;
using Dinoza.Service.Common;
using Dinoza.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.WebApi.Controllers
{
    public class InvoiceController : ApiController
    {
        private readonly IInvoiceService invoiceService;
        public InvoiceController(IInvoiceService invoiceService)
        {
            this.invoiceService = invoiceService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllInvoicesAsync()
        {
            List<GetInvoice> invoices = new List<GetInvoice>();
            List<GetInvoiceRest> invoicesRest = new List<GetInvoiceRest>();
            try
            {
                invoices = await invoiceService.GetInvoicesAsync();
                invoicesRest = SetModelToRest(invoices);
                return Request.CreateResponse(HttpStatusCode.OK, invoicesRest);
            }
            catch (Exception ex)
            {

                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
        [HttpPost]
        public async Task<HttpResponseMessage> AddInvoiceAsync([FromBody] InvoiceRest invoiceRest)
        {
            Invoice invoice = new Invoice();
            invoice = SetModelFromRest(invoiceRest);
            bool result = await invoiceService.AddInvoiceAsync(invoice);
            if (result)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GetSpecificInvoice(int invoiceNumber)
        {
            GetInvoice invoice = new GetInvoice();
            GetInvoiceRest invoiceRest = new GetInvoiceRest();
            try
            {
                invoice = await invoiceService.GetSpecificInvoiceAsync(invoiceNumber);
                List<GetInvoice> invoices = new List<GetInvoice>{
                    invoice
                };
                List<GetInvoiceRest> invoicesRest = new List<GetInvoiceRest>();

                invoicesRest = SetModelToRest(invoices);
                if (invoicesRest[0].InvoiceNumber == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                return Request.CreateResponse(HttpStatusCode.OK, invoicesRest);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> DeleteClientAsync(Guid id)
        {
            int rowsAffected;
            try
            {
                rowsAffected = await invoiceService.DeleteInvoiceAsync(id);
                return Request.CreateResponse(HttpStatusCode.OK, rowsAffected);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut]
        public async Task<HttpResponseMessage> UpdateInvoiceStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods)
        {
            int result;
            try
            {
                result = await invoiceService.UpdateInvoiceStatusAsync(id, updateGoods);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        private List<GetInvoiceRest> SetModelToRest(List<GetInvoice> invoices)
        {
            List<GetInvoiceRest> restInvoices = new List<GetInvoiceRest>();

            foreach (GetInvoice invoice in invoices)
            {

                GetInvoiceRest invoicesRest = new GetInvoiceRest();
                invoicesRest.Id = invoice.Id;
                invoicesRest.InvoiceNumber = invoice.InvoiceNumber;
                invoicesRest.Price = invoice.Price;
                invoicesRest.DateCreated = invoice.DateCreated;
                invoicesRest.InvoiceCreatorLastName = invoice.InvoiceCreatorLastName;
                invoicesRest.InvoiceCreatorName = invoice.InvoiceCreatorName;
                invoicesRest.ManufacturerName = invoice.ManufacturerName;
                invoicesRest.ClientName = invoice.ClientName;
                invoicesRest.StatusName = invoice.StatusName;
                restInvoices.Add(invoicesRest);
            }
            return restInvoices;

        }
        private Invoice SetModelFromRest(InvoiceRest invoiceRest)
        {
            Invoice invoice = new Invoice();
            invoice.Id = Guid.NewGuid();
            invoice.InvoiceNumber = invoiceRest.InvoiceNumber;
            invoice.ClientId = invoiceRest.ClientId;
            invoice.ManufacturerId = invoiceRest.ManufacturerId;
            invoice.InvoiceCreatorLastName = invoiceRest.InvoiceCreatorLastName;
            invoice.InvoiceCreatorName = invoiceRest.InvoiceCreatorName;
            invoice.DateCreated = DateTime.Now;
            invoice.MaterialProductionId = invoiceRest.MaterialProductionId;
            invoice.StatusId = invoiceRest.StatusId;

            return invoice;
        }
    }
}