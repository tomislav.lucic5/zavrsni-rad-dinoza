﻿using Dinoza.Model;
using Dinoza.Service.Common;
using Dinoza.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Dinoza.WebApi.Controllers
{
    public class MaterialController : ApiController
    {
        private readonly IMaterialService materialService;
        public MaterialController(IMaterialService materialService)
        {
            this.materialService = materialService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAlLMaterialsAsync()
        {
            List<Material> material = new List<Material>();
            try
            {
                material = await materialService.GetMaterialsAsync();
                return Request.CreateResponse(HttpStatusCode.OK, material);
            }
            catch (Exception ex)
            {

                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
    }
}