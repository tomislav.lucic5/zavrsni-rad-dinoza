﻿using Dinoza.Service.Common;
using Dinoza.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dinoza.Model;

namespace Dinoza.WebApi.Controllers
{
    public class StatusController : ApiController
    {
        private readonly IStatusService statusService;
        public StatusController(IStatusService statusService)
        {
            this.statusService = statusService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllStatusesAsync()
        {
            List<Status> statuses = new List<Status>();

            try
            {
                statuses = await statusService.GetStatusesAsync();
                return Request.CreateResponse(HttpStatusCode.OK, statuses);
            }
            catch (Exception ex)
            {

                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
    }
}