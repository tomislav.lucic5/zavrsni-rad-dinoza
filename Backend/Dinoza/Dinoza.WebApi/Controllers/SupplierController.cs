﻿using Dinoza.Model;
using Dinoza.Service.Common;
using Dinoza.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Dinoza.WebApi.Controllers
{
    public class SupplierController : ApiController
    {
        private readonly ISupplierService supplierService;
        public SupplierController(ISupplierService supplierService)
        {
            this.supplierService = supplierService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllClientsAsync()
        {
            List<Supplier> suppliers = new List<Supplier>();
            List<SupplierRest> suppliersRest = new List<SupplierRest>();
            try
            {
                suppliers = await supplierService.GetSuppliersAsync();
                suppliersRest = SetModelToRest(suppliers);
                return Request.CreateResponse(HttpStatusCode.OK, suppliersRest);
            }
            catch (Exception ex)
            {

                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
        [HttpPost]
        public async Task<HttpResponseMessage> AddSupplierAsync([FromBody] SupplierRest supplierRest)
        {
            Supplier supplier = new Supplier();
            supplier = SetModelFromRest(supplierRest);
            bool result = await supplierService.AddSupplierAsync(supplier);
            if (result)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GetSpecificInvoice(Guid id)
        {
            Supplier supplier = new Supplier();
            SupplierRest supplierRest = new SupplierRest();
            try
            {
                supplier = await supplierService.GetSpecificSupplierAsync(id);
                List<Supplier> suppliers = new List<Supplier>{
                    supplier
                };
                List<SupplierRest> suppliersRest = new List<SupplierRest>();

                suppliersRest = SetModelToRest(suppliers);
                return Request.CreateResponse(HttpStatusCode.OK, suppliersRest);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        private List<SupplierRest> SetModelToRest(List<Supplier> suppliers)
        {
            List<SupplierRest> suppliersRest = new List<SupplierRest>();

            foreach (Supplier supplier in suppliers)
            {

                SupplierRest supplierRest = new SupplierRest();
                supplierRest.Id = supplier.Id;
                supplierRest.Name = supplier.Name;
                supplierRest.Oib = supplier.Oib;
                supplierRest.Street = supplier.Address.Street;
                supplierRest.Zip = supplier.Address.Zip;
                supplierRest.City = supplier.Address.City;
                supplierRest.Country = supplier.Address.Country;
                suppliersRest.Add(supplierRest);
            }
            return suppliersRest;

        }
        private Supplier SetModelFromRest(SupplierRest supplierRest)
        {
            Supplier supplier = new Supplier();
            supplier.Address = new Address();

            supplier.Id = Guid.NewGuid();
            supplier.Name = supplierRest.Name;
            supplier.Oib = supplierRest.Oib;
            supplier.Address.Street = supplierRest.Street;
            supplier.Address.City = supplierRest.City;
            supplier.Address.Country = supplierRest.Country;
            supplier.Address.Zip = supplierRest.Zip;

            return supplier;
        }
    }
}