﻿using Dinoza.Model;
using Dinoza.Service;
using Dinoza.Service.Common;
using Dinoza.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dinoza.Model.Common;

namespace Dinoza.WebApi.Controllers
{
    public class NotificationController : ApiController
    {
        private readonly INotificationService notificationService;
        public NotificationController(INotificationService notificationService)
        {
            this.notificationService = notificationService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllNotificationsAsync()
        {
            List<Notification> notifications = new List<Notification>();
            try
            {
                notifications = await notificationService.GetNotificationsAsync();
                foreach(var  notification in notifications)
                {
                    if (!notification.IsRead)
                    {
                        notification.UnreadNotifications++;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, notifications);
            }
            catch (Exception ex)
            {

                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
        [HttpPost]
        public async Task<HttpResponseMessage> AddNotificationAsync(Notification notification)
        {
            bool result = await notificationService.AddNotificationAsync(notification);
            if (result)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GetSpecificNotification(Guid id)
        {
            Notification notification = new Notification();
            try
            {
                notification = await notificationService.GetSpecificNotificationAsync(id);

                if (notification == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                return Request.CreateResponse(HttpStatusCode.OK, notification);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        [HttpPut]
        public async Task<HttpResponseMessage> UpdateNotificationAsync(Guid id)
        {
            try
            {
                var result = await notificationService.UpdateNotificationAsync(id);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}