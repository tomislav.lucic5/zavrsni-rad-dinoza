﻿using Dinoza.Model;
using Dinoza.Service;
using Dinoza.Service.Common;
using Dinoza.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Dinoza.WebApi.Controllers
{
    public class GoodsCalculationController : ApiController
    {
        private readonly IGoodsCalculationService goodsCalculationService;
        private readonly INotificationService notificationService;
        public GoodsCalculationController(IGoodsCalculationService goodsCalculationService, INotificationService notificationService)
        {
            this.goodsCalculationService = goodsCalculationService;
            this.notificationService = notificationService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllGoodsCalculationsAsync()
        {
            List<GoodsCalculation> goodsCalculations = new List<GoodsCalculation>();
            List<GoodsCalculationRest> goodsCalculationRests = new List<GoodsCalculationRest>();

            HashSet<DateTime> sentNotificationDates = new HashSet<DateTime>(); // Move this line outside of the if statement

            try
            {
                goodsCalculations = await goodsCalculationService.GetGoodsCalculationsAsync();
                goodsCalculationRests = SetModelToRest(goodsCalculations);
                return Request.CreateResponse(HttpStatusCode.OK, goodsCalculationRests);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }


        [HttpPost]
        public async Task<HttpResponseMessage> AddGoodsCalculationAsync([FromBody] GoodsCalculationRest goodsCalculationRest)
        {
            GoodsCalculation goodsCalculation = new GoodsCalculation();
            goodsCalculation = SetModelFromRest(goodsCalculationRest);
            bool result = await goodsCalculationService.AddGoodsCalculationAsync(goodsCalculation);
            if (result)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GetSpecificInvoice(Guid id)
        {
            GoodsCalculation goodsCalculation = new GoodsCalculation();
            GoodsCalculationRest goodsCalculationRest = new GoodsCalculationRest();
            try
            {
                goodsCalculation = await goodsCalculationService.GetSpecificGoodsCalculationAsync(id);
                List<GoodsCalculation> goodsCalculations = new List<GoodsCalculation>{
                    goodsCalculation
                };
                List<GoodsCalculationRest> goodsCalculationsRest = new List<GoodsCalculationRest>();

                goodsCalculationsRest = SetModelToRest(goodsCalculations);
                if (goodsCalculationsRest != null)
                {
                    TimeSpan timeUntilDeadline = goodsCalculation.PaymentDeadline - DateTime.Now;

                    if (timeUntilDeadline.TotalDays < 0) 
                    {
                        Notification notification = new Notification();
                        notification.Id = Guid.NewGuid();
                        notification.Title = "Payment Deadline Is Overdue!";
                        notification.Body = $"Payment Deadline of order #{goodsCalculation.OrderNumber} was due " +
                            $"{Math.Abs(((int)timeUntilDeadline.TotalDays))} days ago";
                        notification.Importance = "Urgent";
                        notification.DateCreated = DateTime.Now;
                        notification.DateUpdated = DateTime.Now;

                        await notificationService.AddNotificationAsync(notification);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, goodsCalculationsRest);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut]
        public async Task<HttpResponseMessage> UpdateGoodsCalculationStatus(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods)
        {
            int result;
            try
            {
                result = await goodsCalculationService.UpdateGoodsCalculationStatusAsnyc(id, updateGoods);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        private List<GoodsCalculationRest> SetModelToRest(List<GoodsCalculation> goodsCalculations)
        {
            List<GoodsCalculationRest> goodsCalculationRests = new List<GoodsCalculationRest>();

            foreach (GoodsCalculation goodsCalculation in goodsCalculations)
            {

                GoodsCalculationRest goodsCalculationRest = new GoodsCalculationRest();
                goodsCalculationRest.Id = goodsCalculation.Id;
                goodsCalculationRest.SupplierName= goodsCalculation.SupplierName;
                goodsCalculationRest.StatusName = goodsCalculation.StatusName;
                goodsCalculationRest.OrderNumber = goodsCalculation.OrderNumber;
                goodsCalculationRest.Price = goodsCalculation.Price;
                goodsCalculationRest.Amount = goodsCalculation.Amount;
                goodsCalculationRest.DateOfReceipt = goodsCalculation.DateOfReceipt;
                goodsCalculationRest.PaymentDeadline = goodsCalculation.PaymentDeadline;
                goodsCalculationRest.Warehouse = goodsCalculation.Warehouse;
                goodsCalculationRest.GoodsClass = goodsCalculation.GoodsClass;
                goodsCalculationRest.JM = goodsCalculation.JM;
                goodsCalculationRest.Name = goodsCalculation.Name;
                goodsCalculationRests.Add(goodsCalculationRest);
            }
            return goodsCalculationRests;

        }
        private GoodsCalculation SetModelFromRest(GoodsCalculationRest goodsCalculationRest)
        {
            GoodsCalculation goodsCalculation = new GoodsCalculation();
            goodsCalculation.Id = Guid.NewGuid();
            goodsCalculation.SupplierId = goodsCalculationRest.SupplierId;
            goodsCalculation.StatusId = goodsCalculationRest.StatusId;
            goodsCalculation.OrderNumber = goodsCalculationRest.OrderNumber;
            goodsCalculation.Price = goodsCalculationRest.Price;
            goodsCalculation.Amount = goodsCalculationRest.Amount;
            goodsCalculation.DateOfReceipt = DateTime.Now;
            goodsCalculation.PaymentDeadline = DateTime.Now.AddDays(30);
            goodsCalculation.Warehouse = goodsCalculationRest.Warehouse;
            goodsCalculation.GoodsClass = goodsCalculationRest.GoodsClass;
            goodsCalculation.JM = goodsCalculationRest.JM;
            goodsCalculation.Name = goodsCalculationRest.Name;

            return goodsCalculation;
        }
    }
}