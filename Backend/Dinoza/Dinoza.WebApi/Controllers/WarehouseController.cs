﻿using Dinoza.Model;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dinoza.Service;

namespace Dinoza.WebApi.Controllers
{
    public class WarehouseController : ApiController
    {
        private readonly IWarehouseService warehouseService;
        public WarehouseController(IWarehouseService warehouseService)
        {
            this.warehouseService = warehouseService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAlLMaterialsAsync()
        {
            List<Warehouse> warehouses = new List<Warehouse>();
            try
            {
                warehouses = await warehouseService.GetWarehousesAsync();
                return Request.CreateResponse(HttpStatusCode.OK, warehouses);
            }
            catch (Exception ex)
            {

                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
        [HttpPut]
        public async Task<HttpResponseMessage> UpdateWarehouseCapacityAsync(Guid id, int capacity)
        {
            int result;
            try
            {
                result = await warehouseService.UpdateWarehouseCapacityAsync(id, capacity);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/Warehouse/{id}/removecapacity")]
        [HttpPut]
        public async Task<HttpResponseMessage> UpdateWarehouseRemoveCapacityAsync(Guid id, int capacity)
        {
            int result;
            try
            {
                result = await warehouseService.UpdateWarehouseRemoveCapacityAsync(id, capacity);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}