﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dinoza.WebApi.Models
{
    public class GoodsCalculationRest
    {
        public Guid Id { get; set; }
        public string SupplierName { get; set; }
        public DateTime DateOfReceipt { get; set; }
        public DateTime PaymentDeadline { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string GoodsClass { get; set; }
        public string JM { get; set; }
        public double Amount { get; set; }  
        public int OrderNumber { get; set; }
        public int Warehouse { get; set; }
        public string StatusName { get; set; }
        public Guid StatusId { get; set; }
        public Guid SupplierId { get; set; }
    }
}