﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dinoza.WebApi.Models
{
    public class InvoiceRest
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public int InvoiceNumber { get; set; }
        public Guid ManufacturerId { get; set; }
        public Guid MaterialProductionId { get; set; }
        public DateTime DateCreated { get; set; }
        public string InvoiceCreatorName { get; set; }
        public string InvoiceCreatorLastName { get; set; }
        public Guid? StatusId { get; set; }
    }
}