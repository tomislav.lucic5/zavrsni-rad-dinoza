﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dinoza.WebApi.Models
{
    public class SupplierRest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Oib { get; set; }
    }
}