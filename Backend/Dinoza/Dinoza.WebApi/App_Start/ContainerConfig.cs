﻿using Autofac.Core;
using Autofac;
using Dinoza.Repository.Common;
using Dinoza.Repository;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac.Integration.WebApi;
using Dinoza.Service;

namespace Dinoza.WebApi.App_Start
{
    public class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<WarehouseService>().As<IWarehouseService>().InstancePerDependency();
            builder.RegisterType<WarehouseRepository>().As<IWarehouseRepository>().InstancePerDependency();
            builder.RegisterType<NotificationService>().As<INotificationService>().InstancePerDependency();
            builder.RegisterType<NotificationRepository>().As<INotificationRepository>().InstancePerDependency();
            builder.RegisterType<MaterialService>().As<IMaterialService>().InstancePerDependency();
            builder.RegisterType<MaterialRepository>().As<IMaterialRepository>().InstancePerDependency();
            builder.RegisterType<MaterialProductionService>().As<IMaterialProductionService>().InstancePerDependency();
            builder.RegisterType <MaterialProductionRepository>().As<IMaterialProductionRepository>().InstancePerDependency();
            builder.RegisterType<StatusService>().As<IStatusService>().InstancePerDependency();
            builder.RegisterType<StatusRepository>().As<IStatusRepository>().InstancePerDependency();
            builder.RegisterType<GoodsCalculationService>().As<IGoodsCalculationService>().InstancePerDependency();
            builder.RegisterType<GoodsCalculationRepository>().As<IGoodsCalculationRepository>().InstancePerDependency();
            builder.RegisterType<ClientService>().As<IClientService>().InstancePerDependency();
            builder.RegisterType<ClientRepository>().As<IClientRepository>().InstancePerDependency();
            builder.RegisterType<SupplierService>().As<ISupplierService>().InstancePerDependency();
            builder.RegisterType<SupplierRepository>().As<ISupplierRepository>().InstancePerDependency();
            builder.RegisterType<InvoiceRepository>().As<IInvoiceRepository>().InstancePerDependency();
            builder.RegisterType<InvoiceService>().As<IInvoiceService>().InstancePerDependency();
            builder.RegisterType<RegistrationService>().As<IRegistrationService>().InstancePerDependency();
            builder.RegisterType<RegistrationRepository>().As<IRegistrationRepository>().InstancePerDependency();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerDependency();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerDependency();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).InstancePerDependency();


            return builder.Build();
        }
    }
}