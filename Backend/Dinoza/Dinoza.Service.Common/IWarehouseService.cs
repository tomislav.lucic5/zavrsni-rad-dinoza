﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service.Common
{
    public interface IWarehouseService
    {
        Task<List<Warehouse>> GetWarehousesAsync();
        Task<int> UpdateWarehouseCapacityAsync(Guid id, int capacity);
        Task<int> UpdateWarehouseRemoveCapacityAsync(Guid id, int capacity);
    }
}
