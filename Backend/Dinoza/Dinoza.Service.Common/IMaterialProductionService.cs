﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service.Common
{
    public interface IMaterialProductionService
    {
        Task<List<MaterialProduction>> GetMaterialProductionsAsync();
        Task<MaterialProduction> GetSpecificMaterialProductionAsync(Guid id);
        Task<bool> AddMaterialProductionAsync([FromBody] MaterialProduction materialProduction);
        Task<int> UpdateMaterialProductionStatusAsnyc(Guid id, [FromBody] UpdateGoodsCalculationModel updateMaterialProduction);
    }
}
