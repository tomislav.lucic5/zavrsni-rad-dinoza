﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service.Common
{
    public interface ISupplierService
    {
        Task<List<Supplier>> GetSuppliersAsync();
        Task<Supplier> GetSpecificSupplierAsync(Guid id);
        Task<bool> AddSupplierAsync([FromBody] Supplier supplier);
    }
}

