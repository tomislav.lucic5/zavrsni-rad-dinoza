﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service.Common
{
    public interface IClientService
    {
        Task<List<Client>> GetClientsAsync();
        Task<Client> GetSpecificClientAsync(Guid id);
        Task<int> UpdateClientAsync(Guid id, [FromBody] Client client);
        Task<int> DeleteClientAsync(Guid id);
        Task<bool> AddClientAsync([FromBody] Client client);
    }
}
