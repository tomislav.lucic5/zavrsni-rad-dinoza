﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Service.Common
{
    public interface IStatusService
    {
        Task<List<Status>> GetStatusesAsync();
    }
}
