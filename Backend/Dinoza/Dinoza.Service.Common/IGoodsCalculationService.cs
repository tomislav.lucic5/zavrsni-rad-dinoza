﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service.Common
{
    public interface IGoodsCalculationService
    {
        Task<List<GoodsCalculation>> GetGoodsCalculationsAsync();
        Task<GoodsCalculation> GetSpecificGoodsCalculationAsync(Guid id);
        Task<bool> AddGoodsCalculationAsync([FromBody] GoodsCalculation goodsCalculation);
        Task<int> UpdateGoodsCalculationStatusAsnyc(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods);
    }
}
