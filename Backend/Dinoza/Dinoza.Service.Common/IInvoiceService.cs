﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service.Common
{
    public interface IInvoiceService
    {
        Task<List<GetInvoice>> GetInvoicesAsync();
        Task<GetInvoice> GetSpecificInvoiceAsync(int invoiceNumber);
        Task<int> DeleteInvoiceAsync(Guid id);
        Task<bool> AddInvoiceAsync([FromBody] Invoice invoice);
        Task<int> UpdateInvoiceStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods);
    }
}
