﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service.Common
{
    public interface IRegistrationService
    {
        Task<User> LoginUserAsync(string email, string password);
        Task<string> RegisterUserAsync([FromBody] User user);
    }
}
