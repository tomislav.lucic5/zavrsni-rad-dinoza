﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service.Common
{
    public interface INotificationService
    {
        Task<List<Notification>> GetNotificationsAsync();
        Task<Notification> GetSpecificNotificationAsync(Guid id);
        Task<bool> AddNotificationAsync(Notification notification);
        Task<int> UpdateNotificationAsync(Guid id);
    }
}
