﻿using Dinoza.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class Client : IClient
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string OIB { get; set; }
        public IAddress Address { get; set; }
        public string PaymentType { get; set; }
    }
}
