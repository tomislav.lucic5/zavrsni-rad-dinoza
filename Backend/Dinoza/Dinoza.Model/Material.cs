﻿using Dinoza.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class Material : IMaterial
    {
        public Guid Id { get; set; }
        public int MaterialNumber { get; set; }
        public float Price { get; set; }
        public string Name { get; set; }
        public string JM { get; set; }
    }
}
