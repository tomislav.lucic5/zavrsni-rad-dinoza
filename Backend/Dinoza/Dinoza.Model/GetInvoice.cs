﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class GetInvoice
    {   
        public Guid Id { get; set; }
        public int InvoiceNumber { get; set; }
        public string ClientName { get; set; }
        public string ManufacturerName { get; set; }
        public double Price { get; set; }
        public DateTime DateCreated { get; set; }
        public string InvoiceCreatorName { get; set; }
        public string InvoiceCreatorLastName { get; set; }
        public string StatusName { get; set; }
    }
}
