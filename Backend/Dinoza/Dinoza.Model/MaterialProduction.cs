﻿using Dinoza.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class MaterialProduction : IMaterialProduction
    {
        public Guid Id { get; set; }
        public int ProductionNumber { get; set; }
        public Guid GoodsCalculationId { get; set; }
        public Guid MaterialId { get; set; }
        public DateTime DateCreated { get; set; }
        public double ResourcePrice { get; set; }
        public Guid? StatusId { get; set; }
        public string StatusName { get; set; }
        public string MaterialName { get; set; }
        public string GoodsName { get; set; }
        public int Warehouse { get; set; }
        public int OrderNumber { get; set; }
    }
}
