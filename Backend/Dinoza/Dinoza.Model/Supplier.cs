﻿using Dinoza.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class Supplier : ISupplier
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Oib { get; set; }
        public IAddress Address { get; set; }
    }
}
