﻿using Dinoza.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class Manufacturer : IManufacturer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string OIB { get; set; }
        public IAddress Address { get; set; }
    }
}
