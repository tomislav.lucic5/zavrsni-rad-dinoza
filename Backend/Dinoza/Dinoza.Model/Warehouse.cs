﻿using Dinoza.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class Warehouse : IWarehouse
    {
       public Guid Id { get; set; }
       public string Name { get; set; }
       public int Capacity { get; set; }
    }
}
