﻿using Dinoza.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class Invoice : IInvoice
    {
        public Guid Id { get; set; }
        public int InvoiceNumber { get; set; }
        public Guid ClientId { get; set; }
        public Guid MaterialProductionId { get; set; }
        public Guid ManufacturerId { get; set; }
        public DateTime DateCreated { get; set; }
        public string InvoiceCreatorName {  get; set; }
        public string InvoiceCreatorLastName { get; set; }
        public Guid? StatusId { get; set; }
    }
}
