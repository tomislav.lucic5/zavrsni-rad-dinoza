﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model
{
    public class UpdateGoodsCalculationModel
    {
        public Guid? StatusId { get; set; }
        public int? Warehouse { get; set; }
    }
}
