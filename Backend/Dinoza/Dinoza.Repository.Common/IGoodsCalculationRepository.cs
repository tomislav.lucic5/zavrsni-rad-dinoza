﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Repository.Common
{
    public interface IGoodsCalculationRepository
    {
        Task<List<GoodsCalculation>> GetAllGoodsCalculationsAsync();
        Task<GoodsCalculation> GetGoodsCalculationByIdAsync(Guid id);

        Task<bool> AddGoodCalculationAsync(GoodsCalculation goodsCalculation);
        Task<int> UpdateGoodsCalculationStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods);

    }
}
