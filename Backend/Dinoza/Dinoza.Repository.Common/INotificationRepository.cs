﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository.Common
{
    public interface INotificationRepository
    {
        Task<List<Notification>> GetAllNotificationsAsnyc();
        Task<Notification> GetNotificationByIdAsync(Guid id);

        Task<bool> AddNotificationAsync(Notification notification);

        Task<int> UpdateNotificationAsync(Guid id);
    }
}
