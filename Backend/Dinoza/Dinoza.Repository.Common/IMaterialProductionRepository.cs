﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Repository.Common
{
    public interface IMaterialProductionRepository
    {
        Task<List<MaterialProduction>> GetAllMaterialProductionsAsync();
        Task<MaterialProduction> GetMaterialProductionByIdAsync(Guid id);

        Task<bool> AddMaterialProductionAsync(MaterialProduction materialProduction);
        Task<int> UpdateMaterialProductionStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateMaterialProduction);

    }
}
