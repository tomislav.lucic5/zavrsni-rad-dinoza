﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository.Common
{
    public interface IClientRepository
    {
        Task<List<Client>> GetAllClientsAsnyc();
        Task<Client> GetClientByIdAsync(Guid id);

        Task<bool> AddClientAsync(Client client);

        Task<int> RemoveClientAsync(Guid id);

        Task<int> UpdateClientAsync(Guid id, Client updatedClient);

    }
}
