﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Repository.Common
{
    public interface IInvoiceRepository
    {
        Task<List<GetInvoice>> GetAllInvoicesAsnyc();
        Task<GetInvoice> GetInvoiceByIdAsync(int invoiceNumber);

        Task<bool> AddInvoiceAsync(Invoice invoice);
        Task<int> UpdateInvoiceStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods);
        Task<int> RemoveInvoiceAsync(Guid id);

    }
}
