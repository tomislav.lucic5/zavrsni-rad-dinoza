﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository.Common
{
    public interface ISupplierRepository
    {
        Task<List<Supplier>> GetAllSuppliersAsnyc();
        Task<Supplier> GetSupplierByIdAsync(Guid id);

        Task<bool> AddSupplierAsync(Supplier supplier);
    }
}
