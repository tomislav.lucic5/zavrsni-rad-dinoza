﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Repository.Common
{
    public interface IRegistrationRepository
    {
        Task<string> RegisterAsync([FromBody] User user);
        Task<User> LoginAsync(string email, string password);
    }
}
