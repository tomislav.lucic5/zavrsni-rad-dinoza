﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository.Common
{
    public interface IMaterialRepository
    {
        Task<List<Material>> GetAllMaterialsAsync();
    }
}
