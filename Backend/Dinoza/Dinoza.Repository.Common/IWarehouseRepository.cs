﻿using Dinoza.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository.Common
{
    public interface IWarehouseRepository
    {
        Task<List<Warehouse>> GetAllWarehousesAsync();
        Task<int> UpdateWarehouseCapacityAsync(Guid id, int capacity);
        Task<int> UpdateWarehouseRemoveCapacityAsync(Guid id, int capacity);
    }
}
