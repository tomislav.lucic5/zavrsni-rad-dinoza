﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface IMaterialProduction
    {
        Guid Id { get; set; }
        int ProductionNumber { get; set; }
        Guid GoodsCalculationId { get; set; }
        Guid MaterialId { get; set; }
        DateTime DateCreated { get; set; }
        double ResourcePrice { get; set; }
        Guid? StatusId { get; set; }
    }
}
