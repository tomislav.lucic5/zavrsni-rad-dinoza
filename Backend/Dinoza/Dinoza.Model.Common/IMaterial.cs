﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface IMaterial
    {
        Guid Id { get; set; }
        int MaterialNumber { get; set; }
        float Price { get; set; }
        string Name { get; set; }
        string JM { get; set; }
    }
}
