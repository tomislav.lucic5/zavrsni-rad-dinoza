﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface IManufacturer
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string OIB { get; set; }
        IAddress Address { get; set; }


    }
}
