﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface ISupplier
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string Oib { get; set; }
        IAddress Address { get; set; } 
       
    }
}
