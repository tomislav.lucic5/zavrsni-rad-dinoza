﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface IWarehouse
    {
        Guid Id { get; set; }
        string Name { get; set; }
        int Capacity { get; set; }
    }
}
