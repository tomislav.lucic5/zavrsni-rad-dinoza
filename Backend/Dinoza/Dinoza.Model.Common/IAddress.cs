﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface IAddress
    {
        string Street { get; set; }
        string Zip { get; set; }
        string City { get; set; }
        string Country { get; set; }
    }
}
