﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface INotification
    {
        Guid Id { get; set; }
        string Title { get; set; }
        string Body { get; set; }
        string Importance { get; set; }
        DateTime DateCreated { get; set; }
        DateTime DateUpdated { get; set; }
        bool IsRead { get; set; }
    }
}
