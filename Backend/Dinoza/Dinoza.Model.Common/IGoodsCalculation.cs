﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface IGoodsCalculation
    {
        Guid Id { get; set; }
        Guid SupplierId { get; set; }
        DateTime DateOfReceipt { get; set; }
        DateTime PaymentDeadline { get; set; }
        string Name { get; set; }
        double Price { get; set; }
        string GoodsClass { get; set; }
        string JM { get; set; }
        double Amount { get; set; }
        int OrderNumber { get; set; }
        int Warehouse { get; set; }
        Guid StatusId { get; set; }
    }
}
