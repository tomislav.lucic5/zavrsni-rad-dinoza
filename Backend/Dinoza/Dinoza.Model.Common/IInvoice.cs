﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Model.Common
{
    public interface IInvoice
    {
        Guid Id { get; set; }
        int InvoiceNumber { get; set; }
        Guid ClientId { get; set; }
        Guid MaterialProductionId { get; set; }
        Guid ManufacturerId { get; set; }
        DateTime DateCreated { get; set; }
        string InvoiceCreatorName { get; set; }
        string InvoiceCreatorLastName { get; set; }
    }
}
