﻿using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Model.Common;
using Dinoza.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Repository
{
    public class MaterialProductionRepository : IMaterialProductionRepository
    {
        public async Task<bool> AddMaterialProductionAsync(MaterialProduction materialProduction)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO \"MaterialProduction\" (\"Id\", \"ProductionNumber\", \"GoodsCalculationId\", \"MaterialId\", \"DateCreated\", \"ResourcePrice\", \"StatusId\") VALUES " +
                                                       "(@Id, @ProductionNumber, @GoodsCalculationId, @MaterialId, @DateCreated, @ResourcePrice, @StatusId)", conn);

                cmd.Parameters.AddWithValue("@Id", materialProduction.Id);
                cmd.Parameters.AddWithValue("@ProductionNumber", materialProduction.ProductionNumber);
                cmd.Parameters.AddWithValue("@GoodsCalculationId", materialProduction.GoodsCalculationId);
                cmd.Parameters.AddWithValue("@MaterialId", materialProduction.MaterialId);
                cmd.Parameters.AddWithValue("@DateCreated", materialProduction.DateCreated);
                cmd.Parameters.AddWithValue("@ResourcePrice", materialProduction.ResourcePrice);
                cmd.Parameters.AddWithValue("@StatusId", materialProduction.StatusId);

                conn.Open();

                int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();

                if (numberOfAffectedRows > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<List<MaterialProduction>> GetAllMaterialProductionsAsync()
        {
            List <MaterialProduction> materialProductions = new List<MaterialProduction>();

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    await connection.OpenAsync();

                    using (NpgsqlCommand cmd = new NpgsqlCommand(
                       "SELECT " +
                        "MP.\"Id\", " +
                        "MP.\"ProductionNumber\", " +
                        "MP.\"DateCreated\", " +
                        "MP.\"ResourcePrice\", " +
                        "GC.\"OrderNumber\", " +
                        "GC.\"Name\" AS \"GoodsName\", " +
                        "GC.\"Warehouse\", " +
                        "M.\"Name\" AS \"MaterialName\", " +
                        "ST.\"Name\" AS \"StatusName\" " +
                        "FROM " +
                        "\"MaterialProduction\" MP " +
                        "JOIN " +
                        "\"GoodsCalculation\" GC ON MP.\"GoodsCalculationId\" = GC.\"Id\" " +
                        "JOIN " +
                        "\"Material\" M ON MP.\"MaterialId\" = M.\"Id\" " +
                        "JOIN " +
                        "\"Status\" ST ON MP.\"StatusId\" = ST.\"Id\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                MaterialProduction materialProduction = new MaterialProduction();
                                materialProduction.Id = (Guid)reader["Id"];
                                materialProduction.ProductionNumber = (int)reader["ProductionNumber"];
                                materialProduction.GoodsName = (string)reader["GoodsName"];
                                materialProduction.MaterialName = (string)reader["MaterialName"];
                                materialProduction.DateCreated = (DateTime)reader["DateCreated"];
                                materialProduction.ResourcePrice = (double)reader["ResourcePrice"];
                                materialProduction.StatusName = (string)reader["StatusName"];
                                materialProduction.Warehouse = (int)reader["Warehouse"];
                                materialProduction.OrderNumber = (int)reader["OrderNumber"];

                                materialProductions.Add(materialProduction);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }

            return materialProductions;
        }

        public async Task<MaterialProduction> GetMaterialProductionByIdAsync(Guid id)
        {
            MaterialProduction materialProduction = new MaterialProduction();

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    await connection.OpenAsync();

                    using (NpgsqlCommand cmd = new NpgsqlCommand(
                       "SELECT " +
                        "MP.\"Id\", " +
                        "MP.\"ProductionNumber\", " +
                        "MP.\"DateCreated\", " +
                        "MP.\"ResourcePrice\", " +
                        "GC.\"OrderNumber\", " +
                        "GC.\"Name\" AS \"GoodsName\", " +
                        "GC.\"Warehouse\", " +
                        "M.\"Name\" AS \"MaterialName\", " +
                        "ST.\"Name\" AS \"StatusName\" " +
                        "FROM " +
                        "\"MaterialProduction\" MP " +
                        "JOIN " +
                        "\"GoodsCalculation\" GC ON MP.\"GoodsCalculationId\" = GC.\"Id\" " +
                        "JOIN " +
                        "\"Material\" M ON MP.\"MaterialId\" = M.\"Id\" " +
                        "JOIN " +
                        "\"Status\" ST ON MP.\"StatusId\" = ST.\"Id\"", connection))
                    {
                        cmd.Parameters.AddWithValue("Id", id);

                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                materialProduction.Id = (Guid)reader["Id"];
                                materialProduction.ProductionNumber = (int)reader["ProductionNumber"];
                                materialProduction.GoodsName = (string)reader["GoodsName"];
                                materialProduction.MaterialName = (string)reader["MaterialName"];
                                materialProduction.DateCreated = (DateTime)reader["DateCreated"];
                                materialProduction.ResourcePrice = (double)reader["ResourcePrice"];
                                materialProduction.StatusName = (string)reader["StatusName"];
                                materialProduction.Warehouse = (int)reader["Warehouse"];
                                materialProduction.OrderNumber = (int)reader["OrderNumber"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }

            return materialProduction;
        }

        public async Task<int> UpdateMaterialProductionStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateMaterialProduction)
        {
            int rowsAffected = 0;

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    await connection.OpenAsync();

                    using (NpgsqlCommand command = new NpgsqlCommand())
                    {
                        StringBuilder updateQuery = new StringBuilder("UPDATE \"MaterialProduction\" SET ");

                        if (updateMaterialProduction.StatusId.HasValue)
                        {
                            updateQuery.Append("\"StatusId\" = @StatusId, ");
                            command.Parameters.AddWithValue("@StatusId", updateMaterialProduction.StatusId.Value);
                        }

                        if (updateQuery.Length > 0)
                        {
                            updateQuery.Length -= 2; // Remove the last comma and space
                        }


                        if (updateQuery.Length > 0)
                        {
                            updateQuery.Append(" WHERE \"Id\" = @Id");
                            command.Parameters.AddWithValue("@Id", id);

                            string query = updateQuery.ToString();
                            command.CommandText = query;
                            command.Connection = connection;

                            rowsAffected = await command.ExecuteNonQueryAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }

            return rowsAffected;
        }
    }
}
