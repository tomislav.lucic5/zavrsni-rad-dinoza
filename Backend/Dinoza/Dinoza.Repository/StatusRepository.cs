﻿using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository
{
    public class StatusRepository : IStatusRepository
    {
        public async Task<List<Status>> GetAllStatusesAsync()
        {
            List<Status> statuses = new List<Status>();
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM \"Status\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Status status = new Status();

                                status.Id = (Guid)reader["Id"];
                                status.Name = (string)reader["Name"];
                                statuses.Add(status);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return statuses;
        }
    }
}
