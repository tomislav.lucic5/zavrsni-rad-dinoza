﻿using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Model.Common;
using Dinoza.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository
{
    public class ClientRepository : IClientRepository
    {
        public async Task<bool> AddClientAsync(Client client)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO \"Client\" (\"Id\", \"Name\", \"OIB\", \"Street\", \"Zip\", \"City\", \"Country\", \"PaymentType\") VALUES " +
                                                       "(@Id, @Name, @OIB, @Street, @Zip, @City, @Country, @PaymentType)", conn);

                cmd.Parameters.AddWithValue("@Id", client.Id);
                cmd.Parameters.AddWithValue("@Name", client.Name);
                cmd.Parameters.AddWithValue("@OIB", client.OIB);
                cmd.Parameters.AddWithValue("@Street", client.Address.Street);
                cmd.Parameters.AddWithValue("@Zip", client.Address.Zip);
                cmd.Parameters.AddWithValue("@City", client.Address.City);
                cmd.Parameters.AddWithValue("@Country", client.Address.Country);
                cmd.Parameters.AddWithValue("@PaymentType", client.PaymentType);

                conn.Open();

                int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();

                if (numberOfAffectedRows > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<Client> GetClientByIdAsync(Guid id)
        {
            Client client = new Client();
            try
            {
                client = await GetClientById(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return client;
        }

        public async Task<int> RemoveClientAsync(Guid id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("delete from \"Client\" where \"Id\" = @Id", conn);

                cmd.Parameters.AddWithValue("@Id", id);
                conn.Open();
                int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();
                if (numberOfAffectedRows > 0)
                {
                    return 1;
                }
                return -1;
            }
        }

        public async Task<int> UpdateClientAsync(Guid id, Client client)
        {
            int rowsAffected = 0;
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand command = new NpgsqlCommand())
                    {

                        StringBuilder updateQuery = new StringBuilder("UPDATE \"client\" SET ");

                        if (!string.IsNullOrEmpty(client.Name))
                        {
                            updateQuery.Append("\"Name\" = @Name, ");
                            command.Parameters.AddWithValue("@Name", client.Name);

                        }

                        if (!string.IsNullOrEmpty(client.PaymentType))
                        {
                            updateQuery.Append("\"PaymentType\" = @PaymentType, ");
                            command.Parameters.AddWithValue("@Name", client.PaymentType);

                        }

                        if (updateQuery.Length > 0)
                        {
                            updateQuery.Length -= 2;
                        }

                        updateQuery.Append(" WHERE \"Id\" = @Id");

                        string query = updateQuery.ToString();
                        command.CommandText = query;
                        command.Connection = connection;
                        command.Parameters.AddWithValue("Id", @id);
                        rowsAffected = await command.ExecuteNonQueryAsync();

                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                throw;
            }

            return rowsAffected;
        }

        public async Task<List<Client>> GetAllClientsAsnyc()
        {
            List<Client> clients = new List<Client>();
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM \"Client\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Client client = new Client();
                                client.Address = new Address();

                                client.Id = (Guid)reader["Id"];
                                client.Name = (string)reader["Name"];
                                client.OIB = (string)reader["OIB"];
                                client.Address.Street = (string)reader["Street"];
                                client.Address.Zip = (string)reader["Zip"];
                                client.Address.City = (string)reader["City"];
                                client.Address.Country = (string)reader["Country"];
                                client.PaymentType = (string)reader["PaymentType"];

                                clients.Add(client);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return clients;
        }

        private async Task<Client> GetClientById(Guid id)
        {
            Client client = new Client();
            using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand("Select * From \"Client\" WHERE \"Id\" = @id", connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    using (NpgsqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        reader.Read();
                        client.Address = new Address();

                        client.Id = (Guid)reader["Id"];
                        client.Name = (string)reader["Name"];
                        client.OIB = (string)reader["OIB"];
                        client.Address.Street = (string)reader["Street"];
                        client.Address.Zip = (string)reader["Zip"];
                        client.Address.City = (string)reader["City"];
                        client.Address.Country = (string)reader["Country"];
                        client.PaymentType = (string)reader["PaymentType"];
                    }
                }
                return client;
            }
        }
    }
}
