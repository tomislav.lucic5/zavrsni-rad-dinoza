﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Model.Common;
using Dinoza.Repository.Common;
using Npgsql;

namespace Dinoza.Repository
{
    public class GoodsCalculationRepository : IGoodsCalculationRepository
    {
        public async Task<bool> AddGoodCalculationAsync(GoodsCalculation goodsCalculation)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO \"GoodsCalculation\" (\"Id\", \"SupplierId\", \"DateOfReceipt\", \"PaymentDeadline\", \"Name\", \"Price\", \"GoodsClass\", \"JM\", \"Amount\", \"OrderNumber\", \"Warehouse\", \"StatusId\") VALUES " +
                                                       "(@Id, @SupplierId, @DateOfReceipt, @PaymentDeadline, @Name, @Price, @GoodsClass, @Jm, @Amount, @OrderNumber, @Warehouse, @StatusId)", conn);

                cmd.Parameters.AddWithValue("@Id", goodsCalculation.Id);
                cmd.Parameters.AddWithValue("@SupplierId", goodsCalculation.SupplierId);
                cmd.Parameters.AddWithValue("@DateOfReceipt", goodsCalculation.DateOfReceipt);
                cmd.Parameters.AddWithValue("@PaymentDeadline", goodsCalculation.PaymentDeadline);
                cmd.Parameters.AddWithValue("@Name", goodsCalculation.Name);
                cmd.Parameters.AddWithValue("@Price", goodsCalculation.Price);
                cmd.Parameters.AddWithValue("@GoodsClass", goodsCalculation.GoodsClass);
                cmd.Parameters.AddWithValue("@JM", goodsCalculation.JM);
                cmd.Parameters.AddWithValue("@Amount", goodsCalculation.Amount);
                cmd.Parameters.AddWithValue("@OrderNumber", goodsCalculation.OrderNumber);
                cmd.Parameters.AddWithValue("@Warehouse", goodsCalculation.Warehouse);
                cmd.Parameters.AddWithValue("@StatusId", goodsCalculation.StatusId);

                conn.Open();

                int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();

                if (numberOfAffectedRows > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<List<GoodsCalculation>> GetAllGoodsCalculationsAsync()
        {
            List<GoodsCalculation> goodsCalculations = new List<GoodsCalculation>();

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    await connection.OpenAsync();

                    using (NpgsqlCommand cmd = new NpgsqlCommand(
    "SELECT " +
        "GC.\"Id\" AS \"GoodsCalculationId\", " +
        "ST.\"Name\" AS \"StatusName\", " +
        "GC.\"DateOfReceipt\", " +
        "GC.\"PaymentDeadline\", " +
        "GC.\"Name\" AS \"GoodsCalculationName\", " +
        "GC.\"Price\", " +
        "GC.\"GoodsClass\", " +
        "GC.\"Amount\", " +
        "GC.\"JM\", " +
        "GC.\"OrderNumber\", " +
        "GC.\"Warehouse\", " +
        "S.\"Name\" AS \"SupplierName\" " +
    "FROM " +
        "\"GoodsCalculation\" GC " +
    "JOIN " +
        "\"Supplier\" S ON GC.\"SupplierId\" = S.\"Id\" " +
    "JOIN " +
        "\"Status\" ST ON GC.\"StatusId\" = ST.\"Id\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                GoodsCalculation goodsCalculation = new GoodsCalculation();
                                goodsCalculation.Id = (Guid)reader["GoodsCalculationId"];
                                goodsCalculation.SupplierName = (string)reader["SupplierName"];
                                goodsCalculation.DateOfReceipt = (DateTime)reader["DateOfReceipt"];
                                goodsCalculation.PaymentDeadline = (DateTime)reader["PaymentDeadline"];
                                goodsCalculation.Name = (string)reader["GoodsCalculationName"];
                                goodsCalculation.Price = (double)reader["Price"];
                                goodsCalculation.GoodsClass = (string)reader["GoodsClass"];
                                goodsCalculation.Amount = (double)reader["Amount"];
                                goodsCalculation.JM = (string)reader["JM"];
                                goodsCalculation.OrderNumber = (int)reader["OrderNumber"];
                                goodsCalculation.Warehouse = (int)reader["Warehouse"];
                                goodsCalculation.StatusName = (string)reader["StatusName"];

                                goodsCalculations.Add(goodsCalculation);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }

            return goodsCalculations;
        }


        public async Task<GoodsCalculation> GetGoodsCalculationByIdAsync(Guid id)
        {
            GoodsCalculation goodsCalculation = null;

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    await connection.OpenAsync();

                    using (NpgsqlCommand cmd = new NpgsqlCommand(
                        "SELECT " +
                            "GC.\"Id\" AS \"GoodsCalculationId\", " +
                            "S.\"Name\" AS \"SupplierName\", " +
                            "GC.\"DateOfReceipt\", " +
                            "GC.\"PaymentDeadline\", " +
                            "GC.\"Name\" AS \"GoodsCalculationName\", " +
                            "GC.\"Price\", " +
                            "GC.\"GoodsClass\", " +
                            "GC.\"Amount\", " +
                            "GC.\"JM\", " +
                            "GC.\"OrderNumber\", " +
                            "GC.\"Warehouse\", " +
                            "ST.\"Name\" AS \"StatusName\" " +
                        "FROM " +
                            "\"GoodsCalculation\" GC " +
                        "JOIN " +
                            "\"Supplier\" S ON GC.\"SupplierId\" = S.\"Id\" " +
                        "JOIN " +
                            "\"Status\" ST ON GC.\"StatusId\" = ST.\"Id\" " +
                        "WHERE " +
                            "GC.\"Id\" = @Id", connection))
                    {
                        cmd.Parameters.AddWithValue("Id", id);

                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                goodsCalculation = new GoodsCalculation();
                                goodsCalculation.Id = (Guid)reader["GoodsCalculationId"];
                                goodsCalculation.SupplierName = (string)reader["SupplierName"];
                                goodsCalculation.DateOfReceipt = (DateTime)reader["DateOfReceipt"];
                                goodsCalculation.PaymentDeadline = (DateTime)reader["PaymentDeadline"];
                                goodsCalculation.Name = (string)reader["GoodsCalculationName"];
                                goodsCalculation.Price = (double)reader["Price"];
                                goodsCalculation.GoodsClass = (string)reader["GoodsClass"];
                                goodsCalculation.Amount = (double)reader["Amount"];
                                goodsCalculation.JM = (string)reader["JM"];
                                goodsCalculation.OrderNumber = (int)reader["OrderNumber"];
                                goodsCalculation.Warehouse = (int)reader["Warehouse"];
                                goodsCalculation.StatusName = (string)reader["StatusName"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }

            return goodsCalculation;
        }


        public async Task<int> UpdateGoodsCalculationStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods)
        {
            int rowsAffected = 0;

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    await connection.OpenAsync();

                    using (NpgsqlCommand command = new NpgsqlCommand())
                    {
                        StringBuilder updateQuery = new StringBuilder("UPDATE \"GoodsCalculation\" SET ");

                            if (updateGoods.StatusId.HasValue)
                            {
                                updateQuery.Append("\"StatusId\" = @StatusId, ");
                                command.Parameters.AddWithValue("@StatusId", updateGoods.StatusId.Value);
                            }

                            if (updateGoods.Warehouse.HasValue)
                            {
                                updateQuery.Append("\"Warehouse\" = @Warehouse, ");
                                command.Parameters.AddWithValue("@Warehouse", updateGoods.Warehouse.Value);
                            }

                        if (updateQuery.Length > 0)
                        {
                            updateQuery.Length -= 2; // Remove the last comma and space
                        }
                        

                        if (updateQuery.Length > 0)
                        {
                            updateQuery.Append(" WHERE \"Id\" = @Id");
                            command.Parameters.AddWithValue("@Id", id);

                            string query = updateQuery.ToString();
                            command.CommandText = query;
                            command.Connection = connection;

                            rowsAffected = await command.ExecuteNonQueryAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }

            return rowsAffected;
        }



    }
}
