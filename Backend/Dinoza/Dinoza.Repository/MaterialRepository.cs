﻿using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository
{
    public class MaterialRepository : IMaterialRepository
    {
        public async Task<List<Material>> GetAllMaterialsAsync()
        {
            List<Material> materials = new List<Material>();
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM \"Material\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Material material = new Material();

                                material.Id = (Guid)reader["Id"];
                                material.Name = (string)reader["Name"];
                                materials.Add(material);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return materials;
        }
    }
}
