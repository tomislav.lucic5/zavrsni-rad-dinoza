﻿using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository
{
    public class SupplierRepository : ISupplierRepository
    {
        public async Task<bool> AddSupplierAsync(Supplier supplier)
        {
                using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
                {
                    NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO \"Supplier\" (\"Id\", \"Name\", \"Street\", \"Zip\", \"Country\", \"Oib\", \"City\") VALUES " +
                                                           "(@Id, @Name, @Street, @Zip, @Country, @Oib, @City)", conn);

                    cmd.Parameters.AddWithValue("@Id", supplier.Id);
                    cmd.Parameters.AddWithValue("@Name", supplier.Name);
                    cmd.Parameters.AddWithValue("@Street", supplier.Address.Street);
                    cmd.Parameters.AddWithValue("@Zip", supplier.Address.Zip);
                    cmd.Parameters.AddWithValue("@Country", supplier.Address.Country);
                    cmd.Parameters.AddWithValue("@Oib", supplier.Oib);
                    cmd.Parameters.AddWithValue("@City", supplier.Address.City);

                    conn.Open();

                    int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();

                    if (numberOfAffectedRows > 0)
                    {
                        return true;
                    }
                }
                return false;
            }

        public async Task<List<Supplier>> GetAllSuppliersAsnyc()
        {
            List<Supplier> suppliers = new List<Supplier>();
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand cmd = new NpgsqlCommand(
                        "SELECT * FROM \"Supplier\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Supplier supplier = new Supplier();
                                supplier.Address = new Address();
                                supplier.Id = (Guid)reader["Id"];
                                supplier.Name = (string)reader["Name"];
                                supplier.Address.Street = (string)reader["Street"];
                                supplier.Address.City = (string)reader["City"];
                                supplier.Address.Zip = (string)reader["Zip"];
                                supplier.Address.Country = (string)reader["Country"];
                                supplier.Oib = (string)reader["Oib"];

                                suppliers.Add(supplier);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return suppliers;
        }

        public async Task<Supplier> GetSupplierByIdAsync(Guid id)
        {
            Supplier supplier = new Supplier();
            using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM \"Supplier\" WHERE \"Id\" = @id", connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    using (NpgsqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            supplier.Address = new Address();
                            supplier.Id = (Guid)reader["Id"];
                            supplier.Name = (string)reader["Name"];
                            supplier.Address.Street = (string)reader["Street"];
                            supplier.Address.City = (string)reader["City"];
                            supplier.Address.Zip = (string)reader["Zip"];
                            supplier.Address.Country = (string)reader["Country"];
                            supplier.Oib = (string)reader["Oib"];
                        }
                    }
                }
                return supplier;
            }
        }

    }
}
