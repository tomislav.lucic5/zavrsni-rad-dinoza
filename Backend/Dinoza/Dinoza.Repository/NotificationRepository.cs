﻿using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Model.Common;
using Dinoza.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository
{
    public class NotificationRepository : INotificationRepository
    {
        public async Task<bool> AddNotificationAsync(Notification notification)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO \"Notification\" (\"Id\", \"Title\", \"Body\", \"Importance\", \"DateCreated\", \"DateUpdated\", \"IsRead\") VALUES " +
                                                       "(@Id, @Title, @Body, @Importance, @DateCreated, @DateUpdated, @IsRead)", conn);

                cmd.Parameters.AddWithValue("@Id", notification.Id);
                cmd.Parameters.AddWithValue("@Title", notification.Title);
                cmd.Parameters.AddWithValue("@Body", notification.Body);
                cmd.Parameters.AddWithValue("@Importance", notification.Importance);
                cmd.Parameters.AddWithValue("@DateCreated", notification.DateCreated);
                cmd.Parameters.AddWithValue("@DateUpdated", notification.DateUpdated);
                cmd.Parameters.AddWithValue("@IsRead", notification.IsRead);

                conn.Open();

                int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();

                if (numberOfAffectedRows > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<List<Notification>> GetAllNotificationsAsnyc()
        {
            List<Notification> notifications = new List<Notification>();
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    connection.Open();

                    // Retrieve notifications
                    using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM \"Notification\" ORDER BY \"DateCreated\" DESC", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Notification notification = new Notification();

                                notification.Id = (Guid)reader["Id"];
                                notification.Title = (string)reader["Title"];
                                notification.Body = (string)reader["Body"];
                                notification.Importance = (string)reader["Importance"];
                                notification.DateCreated = (DateTime)reader["DateCreated"];
                                notification.DateUpdated = (DateTime)reader["DateUpdated"];
                                notification.IsRead = (bool)reader["IsRead"];
                                notifications.Add(notification);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return notifications;
        }


        public async Task<Notification> GetNotificationByIdAsync(Guid id)
        {
            Notification notification = new Notification();
            using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand(
                    "SELECT * FROM \"Notification\" WHERE \"Id\" = @id", connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    using (NpgsqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            notification.Id = (Guid)reader["Id"];
                            notification.Title = (string)reader["Title"];
                            notification.Body = (string)reader["Body"];
                            notification.Importance = (string)reader["Importance"];
                            notification.DateCreated = (DateTime)reader["DateCreated"];
                            notification.DateUpdated = (DateTime)reader["DateUpdated"];
                            notification.IsRead = (bool)reader["IsRead"];
                        }
                    }
                }
                return notification;
            }
        }

        public async Task<int> UpdateNotificationAsync(Guid id)
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
                {
                    NpgsqlCommand cmd = new NpgsqlCommand("UPDATE \"Notification\" SET \"IsRead\" = TRUE WHERE \"Id\" = @Id", conn);

                    cmd.Parameters.AddWithValue("@Id", id);
                    conn.Open();
                    int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();
                    if (numberOfAffectedRows > 0)
                    {
                        return 1;
                    }
                    return -1;
                }
            } catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
