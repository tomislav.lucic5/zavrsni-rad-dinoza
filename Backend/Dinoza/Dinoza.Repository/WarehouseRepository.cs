﻿using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dinoza.Repository
{
    public class WarehouseRepository : IWarehouseRepository
    {
        public async Task<List<Warehouse>> GetAllWarehousesAsync()
        {
            List<Warehouse> warehouses = new List<Warehouse>();
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM \"Warehouse\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Warehouse warehouse = new Warehouse();

                                warehouse.Id = (Guid)reader["Id"];
                                warehouse.Name = (string)reader["Name"];
                                warehouse.Capacity = (int)reader["Capacity"];
                                warehouses.Add(warehouse);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return warehouses;
        }

        public async Task<int> UpdateWarehouseCapacityAsync(Guid id, int addCapacity)
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
                {
                    NpgsqlCommand cmd = new NpgsqlCommand("UPDATE \"Warehouse\" SET \"Capacity\" = \"Capacity\" + @addCapacity WHERE \"Id\" = @Id", conn);

                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Parameters.AddWithValue("@addCapacity", addCapacity);
                    conn.Open();
                    int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();
                    if (numberOfAffectedRows > 0)
                    {
                        return 1;
                    }
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<int> UpdateWarehouseRemoveCapacityAsync(Guid id, int addCapacity)
        {
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
                {
                    NpgsqlCommand cmd = new NpgsqlCommand("UPDATE \"Warehouse\" SET \"Capacity\" = \"Capacity\" - @addCapacity WHERE \"Id\" = @Id", conn);

                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Parameters.AddWithValue("@addCapacity", addCapacity);
                    conn.Open();
                    int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();
                    if (numberOfAffectedRows > 0)
                    {
                        return 1;
                    }
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
