﻿using Dinoza.Common;
using Dinoza.Model;
using Dinoza.Model.Common;
using Dinoza.Repository.Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Repository
{
    public class InvoiceRepository : IInvoiceRepository
    {
        public async Task<bool> AddInvoiceAsync(Invoice invoice)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO \"Invoice\" (\"Id\", \"InvoiceNumber\", \"ClientId\", \"MaterialProductionId\", \"ManufacturerId\", \"DateCreated\", \"InvoiceCreatorName\", \"InvoiceCreatorLastName\", \"StatusId\") VALUES " +
                                                       "(@Id, @InvoiceNumber, @ClientId, @MaterialProductionId, @ManufacturerId, @DateCreated, @InvoiceCreatorName, @InvoiceCreatorLastName, @StatusId)", conn);

                cmd.Parameters.AddWithValue("@Id", invoice.Id);
                cmd.Parameters.AddWithValue("@InvoiceNumber", invoice.InvoiceNumber);
                cmd.Parameters.AddWithValue("@ClientId", invoice.ClientId);
                cmd.Parameters.AddWithValue("@MaterialProductionId", invoice.MaterialProductionId);
                cmd.Parameters.AddWithValue("@ManufacturerId", invoice.ManufacturerId);
                cmd.Parameters.AddWithValue("@DateCreated", invoice.DateCreated);
                cmd.Parameters.AddWithValue("@InvoiceCreatorName", invoice.InvoiceCreatorName);
                cmd.Parameters.AddWithValue("@InvoiceCreatorLastName", invoice.InvoiceCreatorLastName);
                cmd.Parameters.AddWithValue("@StatusId", invoice.StatusId);

                conn.Open();

                int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();

                if (numberOfAffectedRows > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<List<GetInvoice>> GetAllInvoicesAsnyc()
        {
            List<GetInvoice> invoices = new List<GetInvoice>();
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand cmd = new NpgsqlCommand(
                        "SELECT I.\"Id\" AS InvoiceId, " +
                               "I.\"InvoiceNumber\", " +
                               "I.\"DateCreated\", " +
                               "I.\"InvoiceCreatorLastName\", " +
                               "I.\"InvoiceCreatorName\", " +
                               "C.\"Name\" AS ClientName, " +
                               "M.\"Name\" AS ManufacturerName, " +
                               "MP.\"ResourcePrice\", " +
                               "ST.\"Name\" AS StatusName " +
                        "FROM \"Invoice\" I " +
                        "JOIN \"Client\" C ON I.\"ClientId\" = C.\"Id\" " +
                        "JOIN \"Manufacturer\" M ON I.\"ManufacturerId\" = M.\"Id\" " +
                        "JOIN \"MaterialProduction\" MP ON I.\"MaterialProductionId\" = MP.\"Id\" " + 
                        "JOIN \"Status\" ST on I.\"StatusId\" = ST.\"Id\"", connection))
                    {
                        using (NpgsqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                GetInvoice invoice = new GetInvoice();
                                invoice.Id = (Guid)reader["InvoiceId"];
                                invoice.InvoiceNumber = (int)reader["InvoiceNumber"];
                                invoice.DateCreated = (DateTime)reader["DateCreated"];
                                invoice.InvoiceCreatorLastName = (string)reader["InvoiceCreatorLastName"];
                                invoice.InvoiceCreatorName = (string)reader["InvoiceCreatorName"];
                                invoice.ClientName = (string)reader["ClientName"];
                                invoice.ManufacturerName = (string)reader["ManufacturerName"];
                                invoice.Price = (double)reader["ResourcePrice"];
                                invoice.StatusName = (string)reader["StatusName"];

                                invoices.Add(invoice);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
            return invoices;

        }

        public async Task<GetInvoice> GetInvoiceByIdAsync(int invoiceNumber)
        {
            GetInvoice invoice = new GetInvoice();
            using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand(
                    "SELECT I.\"Id\" AS InvoiceId, " +
                           "I.\"InvoiceNumber\", " +
                           "I.\"DateCreated\", " +
                           "I.\"InvoiceCreatorLastName\", " +
                           "I.\"InvoiceCreatorName\", " +
                           "C.\"Name\" AS ClientName, " +
                           "M.\"Name\" AS ManufacturerName, " +
                           "MP.\"ResourcePrice\" " +
                    "FROM \"Invoice\" I " +
                    "JOIN \"Client\" C ON I.\"ClientId\" = C.\"Id\" " +
                    "JOIN \"Manufacturer\" M ON I.\"ManufacturerId\" = M.\"Id\" " +
                    "JOIN \"MaterialProduction\" MP ON I.\"MaterialProductionId\" = MP.\"Id\" " +
                    "WHERE I.\"InvoiceNumber\" = @invoiceNumber", connection))
                {
                    command.Parameters.AddWithValue("@invoiceNumber", invoiceNumber);
                    using (NpgsqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            invoice.Id = (Guid)reader["InvoiceId"];
                            invoice.InvoiceNumber = (int)reader["InvoiceNumber"];
                            invoice.DateCreated = (DateTime)reader["DateCreated"];
                            invoice.InvoiceCreatorLastName = (string)reader["InvoiceCreatorLastName"];
                            invoice.InvoiceCreatorName = (string)reader["InvoiceCreatorName"];
                            invoice.ClientName = (string)reader["ClientName"];
                            invoice.ManufacturerName = (string)reader["ManufacturerName"];
                            invoice.Price = (double)reader["ResourcePrice"];
                        }
                    }
                }
                return invoice;
            }
        }

        public async Task<int> RemoveInvoiceAsync(Guid id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(Helper.connectionString))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("delete from \"Invoice\" where \"Id\" = @Id", conn);

                cmd.Parameters.AddWithValue("@Id", id);
                conn.Open();
                int numberOfAffectedRows = await cmd.ExecuteNonQueryAsync();
                if (numberOfAffectedRows > 0)
                {
                    return 1;
                }
                return -1;
            }
        }

        public async Task<int> UpdateInvoiceStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods)
        {
            int rowsAffected = 0;

            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(Helper.connectionString))
                {
                    await connection.OpenAsync();

                    using (NpgsqlCommand command = new NpgsqlCommand())
                    {
                        StringBuilder updateQuery = new StringBuilder("UPDATE \"Invoice\" SET ");

                        if (updateGoods.StatusId.HasValue)
                        {
                            updateQuery.Append("\"StatusId\" = @StatusId, ");
                            command.Parameters.AddWithValue("@StatusId", updateGoods.StatusId.Value);
                        }

                        if (updateQuery.Length > 0)
                        {
                            updateQuery.Length -= 2; 
                        }

                        if (updateQuery.Length > 0)
                        {
                            updateQuery.Append(" WHERE \"Id\" = @Id");
                            command.Parameters.AddWithValue("@Id", id);

                            string query = updateQuery.ToString();
                            command.CommandText = query;
                            command.Connection = connection;

                            rowsAffected = await command.ExecuteNonQueryAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }

            return rowsAffected;
        }
    }
}
