﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository notificationRepository;
        public NotificationService(INotificationRepository notificationRepository)
        {
            this.notificationRepository = notificationRepository;
        }
        [HttpPost]
        public async Task<bool> AddNotificationAsync(Notification notification)
        {
            try
            {
                return await notificationRepository.AddNotificationAsync(notification);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
        [HttpGet]
        public async Task<List<Notification>> GetNotificationsAsync()
        {
            try
            {
                return await notificationRepository.GetAllNotificationsAsnyc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<Notification> GetSpecificNotificationAsync(Guid id)
        {
            try
            {
                return await notificationRepository.GetNotificationByIdAsync(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<int> UpdateNotificationAsync(Guid id)
        {
            try
            {
                return await notificationRepository.UpdateNotificationAsync(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
