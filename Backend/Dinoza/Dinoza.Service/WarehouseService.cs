﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service
{
    public class WarehouseService : IWarehouseService
    {
        private readonly IWarehouseRepository warehouseRepository;
        public WarehouseService(IWarehouseRepository warehouseRepository)
        {
            this.warehouseRepository = warehouseRepository;
        }
        [HttpGet]
        public async Task<List<Warehouse>> GetWarehousesAsync()
        {
            try
            {
                return await warehouseRepository.GetAllWarehousesAsync();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<int> UpdateWarehouseCapacityAsync(Guid id, int capacity)
        {
                try
                {
                    var result = await warehouseRepository.UpdateWarehouseCapacityAsync(id, capacity);
                    return result;
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message.ToString());
                    throw;
                }
        }

        public async Task<int> UpdateWarehouseRemoveCapacityAsync(Guid id, int capacity)
        {
            try
            {
                var result = await warehouseRepository.UpdateWarehouseRemoveCapacityAsync(id, capacity);
                return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
