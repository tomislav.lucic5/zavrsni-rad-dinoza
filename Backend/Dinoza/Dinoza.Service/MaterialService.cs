﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service
{
    public class MaterialService : IMaterialService
    {
        private readonly IMaterialRepository materialRepository;
        public MaterialService(IMaterialRepository materialRepository)
        {
            this.materialRepository = materialRepository;
        }
        [HttpGet]
        public async Task<List<Material>> GetMaterialsAsync()
        {
            try
            {
                return await materialRepository.GetAllMaterialsAsync();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
