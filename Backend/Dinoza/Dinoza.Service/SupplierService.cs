﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service
{
    public class SupplierService : ISupplierService
    {
        private readonly ISupplierRepository SupplierRepository;
        public SupplierService(ISupplierRepository SupplierRepository)
        {
            this.SupplierRepository =  SupplierRepository;
        }
        public async Task<bool> AddSupplierAsync([FromBody] Supplier supplier)
        {
            try
            {
                return await SupplierRepository.AddSupplierAsync(supplier);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<Supplier> GetSpecificSupplierAsync(Guid id)
        {
            try
            {
                return await SupplierRepository.GetSupplierByIdAsync(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<List<Supplier>> GetSuppliersAsync()
        {
            try
            {
                return await SupplierRepository.GetAllSuppliersAsnyc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
