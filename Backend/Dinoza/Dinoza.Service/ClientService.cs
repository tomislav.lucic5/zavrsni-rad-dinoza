﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository ClientRepository;
        public ClientService(IClientRepository ClientRepository)
        {
            this.ClientRepository = ClientRepository;
        }
        [HttpPost]
        public async Task<bool> AddClientAsync([FromBody] Client client)
        {
            try
            {
                return await ClientRepository.AddClientAsync(client);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
        [HttpGet]
        public async Task<List<Client>> GetClientsAsync()
        {
            try
            {
                return await ClientRepository.GetAllClientsAsnyc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
        [HttpGet]
        public async Task<Client> GetSpecificClientAsync(Guid id)
        {
            try
            {
                return await ClientRepository.GetClientByIdAsync(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
        [HttpPut]
        public async Task<int> UpdateClientAsync(Guid id, [FromBody] Client Client)
        {
            try
            {
                return await ClientRepository.UpdateClientAsync(id, Client);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
        [HttpDelete]
        public async Task<int> DeleteClientAsync(Guid id)
        {
            try
            {
                return await ClientRepository.RemoveClientAsync(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
