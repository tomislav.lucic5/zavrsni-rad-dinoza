﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service
{
    public class GoodsCalculationService : IGoodsCalculationService
    {
        private readonly IGoodsCalculationRepository goodsCalculationRepository;
        private readonly INotificationRepository notificationRepository;
        private readonly IStatusRepository statusRepository;
        public GoodsCalculationService(IGoodsCalculationRepository goodsCalculationRepository, INotificationRepository notificationRepository, IStatusRepository statusRepository)
        {
            this.goodsCalculationRepository = goodsCalculationRepository;
            this.notificationRepository = notificationRepository;
            this.statusRepository = statusRepository;
        }
        public async Task<bool> AddGoodsCalculationAsync([FromBody] GoodsCalculation goodsCalculation)
        {
            try
            {
                return await goodsCalculationRepository.AddGoodCalculationAsync(goodsCalculation);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<List<GoodsCalculation>> GetGoodsCalculationsAsync()
        {
            try
            {
                var result = await goodsCalculationRepository.GetAllGoodsCalculationsAsync();
                return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<GoodsCalculation> GetSpecificGoodsCalculationAsync(Guid id)
        {
            try
            {
                return await goodsCalculationRepository.GetGoodsCalculationByIdAsync(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<int> UpdateGoodsCalculationStatusAsnyc(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods)
        {
            try
            {
                var goodsCalculation = await goodsCalculationRepository.GetGoodsCalculationByIdAsync(id);
                var result = await goodsCalculationRepository.UpdateGoodsCalculationStatusAsync(id, updateGoods);
                if (result != null)
                {
                    var status = await statusRepository.GetAllStatusesAsync();
                    var goodsCalculationStatus = status.Find(x => x.Id == updateGoods.StatusId).Name;
                    Notification notification = new Notification();
                    notification.Id = Guid.NewGuid();
                    notification.Title = "Order Status Change";
                    notification.Body = $"Order {goodsCalculation.OrderNumber} status has changed from {goodsCalculation.StatusName} to {goodsCalculationStatus}";
                    notification.Importance = "Normal";
                    notification.DateCreated = DateTime.Now;
                    notification.DateUpdated = DateTime.Now;
                    await notificationRepository.AddNotificationAsync(notification);
                }
                return result;
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
