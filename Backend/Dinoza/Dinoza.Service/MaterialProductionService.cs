﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Dinoza.Service
{
    public class MaterialProductionService : IMaterialProductionService
    {
        private readonly IMaterialProductionRepository materialProductionRepository;
        private readonly INotificationRepository notificationRepository;
        private readonly IStatusRepository statusRepository;
        public MaterialProductionService(IMaterialProductionRepository materialProductionRepository, INotificationRepository notificationRepository, IStatusRepository statusRepository)
        {
            this.materialProductionRepository = materialProductionRepository;
            this.notificationRepository = notificationRepository;
            this.statusRepository = statusRepository;
        }
        public async Task<bool> AddMaterialProductionAsync([FromBody] MaterialProduction materialProduction)
        {
            try
            {
                return await materialProductionRepository.AddMaterialProductionAsync(materialProduction);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<List<MaterialProduction>> GetMaterialProductionsAsync()
        {
            try
            {
                return await materialProductionRepository.GetAllMaterialProductionsAsync();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<MaterialProduction> GetSpecificMaterialProductionAsync(Guid id)
        {
            try
            {
                return await materialProductionRepository.GetMaterialProductionByIdAsync(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        public async Task<int> UpdateMaterialProductionStatusAsnyc(Guid id, [FromBody] UpdateGoodsCalculationModel updateMaterialProduction)
        {
            try
            {
                var materialProduction = await materialProductionRepository.GetMaterialProductionByIdAsync(id);
                int result = await materialProductionRepository.UpdateMaterialProductionStatusAsync(id, updateMaterialProduction);
                if (result != null)
                {
                    var status = await statusRepository.GetAllStatusesAsync();
                    var materialProductionStatus = status.Find(x => x.Id == updateMaterialProduction.StatusId).Name;
                    Notification notification = new Notification();
                    notification.Id = Guid.NewGuid();
                    notification.Title = "Production Status Change";
                    notification.Body = $"Production {materialProduction.ProductionNumber} status has changed from {materialProduction.StatusName} to {materialProductionStatus}";
                    notification.Importance = "Normal";
                    notification.DateCreated = DateTime.Now;
                    notification.DateUpdated = DateTime.Now;
                    await notificationRepository.AddNotificationAsync(notification);
                }
                return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
