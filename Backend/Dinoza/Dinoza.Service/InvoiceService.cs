﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IInvoiceRepository InvoiceRepository;
        private readonly INotificationRepository NotificationRepository;
        private readonly IStatusRepository StatusRepository;
        public InvoiceService(IInvoiceRepository InvoiceRepository, INotificationRepository NotificationRepository, IStatusRepository StatusRepository)
        {
            this.InvoiceRepository = InvoiceRepository;
            this.NotificationRepository = NotificationRepository;
            this.StatusRepository = StatusRepository;
        }

        [HttpPost]
        public async Task<bool> AddInvoiceAsync([FromBody] Invoice invoice)
        {
            try
            {
                return await InvoiceRepository.AddInvoiceAsync(invoice);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        [HttpDelete]
        public async Task<int> DeleteInvoiceAsync(Guid id)
        {
            try
            {
                return await InvoiceRepository.RemoveInvoiceAsync(id);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }

        [HttpGet]
        public async Task<List<GetInvoice>> GetInvoicesAsync()
        {
            try
            {
                return await InvoiceRepository.GetAllInvoicesAsnyc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
        [HttpGet]
        public async Task<GetInvoice> GetSpecificInvoiceAsync(int invoiceNumber)
        {
            try
            {
                return await InvoiceRepository.GetInvoiceByIdAsync(invoiceNumber);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
        [HttpPut]
        public async Task<int> UpdateInvoiceStatusAsync(Guid id, [FromBody] UpdateGoodsCalculationModel updateGoods)
        {
            try
            {
                var result = await InvoiceRepository.UpdateInvoiceStatusAsync(id, updateGoods);
                if (result != null)
                {
                    var status = await StatusRepository.GetAllStatusesAsync();
                    var statusName = status.Find(x => x.Id == updateGoods.StatusId).Name;
                    Notification notification = new Notification();
                    notification.Id = Guid.NewGuid();
                    notification.Title = "Invoice Status Change";
                    notification.Body = $"Invoice status has changed to {statusName}";
                    notification.Importance = "Normal";
                    notification.DateCreated = DateTime.Now;
                    notification.DateUpdated = DateTime.Now;
                    await NotificationRepository.AddNotificationAsync(notification);
                }
                return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
