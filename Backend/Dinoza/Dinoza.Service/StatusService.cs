﻿using Dinoza.Model;
using Dinoza.Repository.Common;
using Dinoza.Service.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dinoza.Service
{
    public class StatusService : IStatusService
    {
        private readonly IStatusRepository StatusRepository;
        public StatusService(IStatusRepository StatusRepository)
        {
            this.StatusRepository = StatusRepository;
        }
        [HttpGet]
        public async Task<List<Status>> GetStatusesAsync()
        {
            try
            {
                return await StatusRepository.GetAllStatusesAsync();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message.ToString());
                throw;
            }
        }
    }
}
